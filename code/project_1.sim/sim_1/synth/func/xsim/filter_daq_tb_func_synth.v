// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.2.1 (lin64) Build 2729669 Thu Dec  5 04:48:12 MST 2019
// Date        : Wed Nov 11 18:43:22 2020
// Host        : mpliax-Inspiron-5593 running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -mode funcsim -nolib -force -file
//               /home/mpliax/Sync/Documents/CTN_ENG/Classes/M119_MultiProcessing_on_IC/Lab/Lab_exercise2/project_1.sim/sim_1/synth/func/xsim/filter_daq_tb_func_synth.v
// Design      : filter_daq
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z020clg484-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "blk_mem_gen_0,blk_mem_gen_v8_4_4,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "blk_mem_gen_v8_4_4,Vivado 2019.2" *) 
module blk_mem_gen_0
   (clka,
    wea,
    addra,
    dina,
    clkb,
    addrb,
    doutb);
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME BRAM_PORTA, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1" *) input clka;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA WE" *) input [0:0]wea;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA ADDR" *) input [2:0]addra;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA DIN" *) input [15:0]dina;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTB CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME BRAM_PORTB, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1" *) input clkb;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTB ADDR" *) input [2:0]addrb;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTB DOUT" *) output [15:0]doutb;

  wire [2:0]addra;
  wire [2:0]addrb;
  wire clka;
  wire [15:0]dina;
  wire [15:0]doutb;
  wire [0:0]wea;
  wire NLW_U0_dbiterr_UNCONNECTED;
  wire NLW_U0_rsta_busy_UNCONNECTED;
  wire NLW_U0_rstb_busy_UNCONNECTED;
  wire NLW_U0_s_axi_arready_UNCONNECTED;
  wire NLW_U0_s_axi_awready_UNCONNECTED;
  wire NLW_U0_s_axi_bvalid_UNCONNECTED;
  wire NLW_U0_s_axi_dbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_rlast_UNCONNECTED;
  wire NLW_U0_s_axi_rvalid_UNCONNECTED;
  wire NLW_U0_s_axi_sbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_wready_UNCONNECTED;
  wire NLW_U0_sbiterr_UNCONNECTED;
  wire [15:0]NLW_U0_douta_UNCONNECTED;
  wire [2:0]NLW_U0_rdaddrecc_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_bid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_bresp_UNCONNECTED;
  wire [2:0]NLW_U0_s_axi_rdaddrecc_UNCONNECTED;
  wire [15:0]NLW_U0_s_axi_rdata_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_rid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_rresp_UNCONNECTED;

  (* C_ADDRA_WIDTH = "3" *) 
  (* C_ADDRB_WIDTH = "3" *) 
  (* C_ALGORITHM = "1" *) 
  (* C_AXI_ID_WIDTH = "4" *) 
  (* C_AXI_SLAVE_TYPE = "0" *) 
  (* C_AXI_TYPE = "1" *) 
  (* C_BYTE_SIZE = "9" *) 
  (* C_COMMON_CLK = "1" *) 
  (* C_COUNT_18K_BRAM = "1" *) 
  (* C_COUNT_36K_BRAM = "0" *) 
  (* C_CTRL_ECC_ALGO = "NONE" *) 
  (* C_DEFAULT_DATA = "0" *) 
  (* C_DISABLE_WARN_BHV_COLL = "0" *) 
  (* C_DISABLE_WARN_BHV_RANGE = "0" *) 
  (* C_ELABORATION_DIR = "./" *) 
  (* C_ENABLE_32BIT_ADDRESS = "0" *) 
  (* C_EN_DEEPSLEEP_PIN = "0" *) 
  (* C_EN_ECC_PIPE = "0" *) 
  (* C_EN_RDADDRA_CHG = "0" *) 
  (* C_EN_RDADDRB_CHG = "0" *) 
  (* C_EN_SAFETY_CKT = "0" *) 
  (* C_EN_SHUTDOWN_PIN = "0" *) 
  (* C_EN_SLEEP_PIN = "0" *) 
  (* C_EST_POWER_SUMMARY = "Estimated Power for IP     :     3.01735 mW" *) 
  (* C_FAMILY = "zynq" *) 
  (* C_HAS_AXI_ID = "0" *) 
  (* C_HAS_ENA = "0" *) 
  (* C_HAS_ENB = "0" *) 
  (* C_HAS_INJECTERR = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_A = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_B = "1" *) 
  (* C_HAS_MUX_OUTPUT_REGS_A = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_REGCEA = "0" *) 
  (* C_HAS_REGCEB = "0" *) 
  (* C_HAS_RSTA = "0" *) 
  (* C_HAS_RSTB = "0" *) 
  (* C_HAS_SOFTECC_INPUT_REGS_A = "0" *) 
  (* C_HAS_SOFTECC_OUTPUT_REGS_B = "0" *) 
  (* C_INITA_VAL = "0" *) 
  (* C_INITB_VAL = "0" *) 
  (* C_INIT_FILE = "blk_mem_gen_0.mem" *) 
  (* C_INIT_FILE_NAME = "no_coe_file_loaded" *) 
  (* C_INTERFACE_TYPE = "0" *) 
  (* C_LOAD_INIT_FILE = "0" *) 
  (* C_MEM_TYPE = "1" *) 
  (* C_MUX_PIPELINE_STAGES = "0" *) 
  (* C_PRIM_TYPE = "1" *) 
  (* C_READ_DEPTH_A = "8" *) 
  (* C_READ_DEPTH_B = "8" *) 
  (* C_READ_LATENCY_A = "1" *) 
  (* C_READ_LATENCY_B = "1" *) 
  (* C_READ_WIDTH_A = "16" *) 
  (* C_READ_WIDTH_B = "16" *) 
  (* C_RSTRAM_A = "0" *) 
  (* C_RSTRAM_B = "0" *) 
  (* C_RST_PRIORITY_A = "CE" *) 
  (* C_RST_PRIORITY_B = "CE" *) 
  (* C_SIM_COLLISION_CHECK = "ALL" *) 
  (* C_USE_BRAM_BLOCK = "0" *) 
  (* C_USE_BYTE_WEA = "0" *) 
  (* C_USE_BYTE_WEB = "0" *) 
  (* C_USE_DEFAULT_DATA = "0" *) 
  (* C_USE_ECC = "0" *) 
  (* C_USE_SOFTECC = "0" *) 
  (* C_USE_URAM = "0" *) 
  (* C_WEA_WIDTH = "1" *) 
  (* C_WEB_WIDTH = "1" *) 
  (* C_WRITE_DEPTH_A = "8" *) 
  (* C_WRITE_DEPTH_B = "8" *) 
  (* C_WRITE_MODE_A = "NO_CHANGE" *) 
  (* C_WRITE_MODE_B = "READ_FIRST" *) 
  (* C_WRITE_WIDTH_A = "16" *) 
  (* C_WRITE_WIDTH_B = "16" *) 
  (* C_XDEVICEFAMILY = "zynq" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  blk_mem_gen_v8_4_4 U0
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(1'b0),
        .dbiterr(NLW_U0_dbiterr_UNCONNECTED),
        .deepsleep(1'b0),
        .dina(dina),
        .dinb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .douta(NLW_U0_douta_UNCONNECTED[15:0]),
        .doutb(doutb),
        .eccpipece(1'b0),
        .ena(1'b0),
        .enb(1'b0),
        .injectdbiterr(1'b0),
        .injectsbiterr(1'b0),
        .rdaddrecc(NLW_U0_rdaddrecc_UNCONNECTED[2:0]),
        .regcea(1'b0),
        .regceb(1'b0),
        .rsta(1'b0),
        .rsta_busy(NLW_U0_rsta_busy_UNCONNECTED),
        .rstb(1'b0),
        .rstb_busy(NLW_U0_rstb_busy_UNCONNECTED),
        .s_aclk(1'b0),
        .s_aresetn(1'b0),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arburst({1'b0,1'b0}),
        .s_axi_arid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(NLW_U0_s_axi_arready_UNCONNECTED),
        .s_axi_arsize({1'b0,1'b0,1'b0}),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awburst({1'b0,1'b0}),
        .s_axi_awid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(NLW_U0_s_axi_awready_UNCONNECTED),
        .s_axi_awsize({1'b0,1'b0,1'b0}),
        .s_axi_awvalid(1'b0),
        .s_axi_bid(NLW_U0_s_axi_bid_UNCONNECTED[3:0]),
        .s_axi_bready(1'b0),
        .s_axi_bresp(NLW_U0_s_axi_bresp_UNCONNECTED[1:0]),
        .s_axi_bvalid(NLW_U0_s_axi_bvalid_UNCONNECTED),
        .s_axi_dbiterr(NLW_U0_s_axi_dbiterr_UNCONNECTED),
        .s_axi_injectdbiterr(1'b0),
        .s_axi_injectsbiterr(1'b0),
        .s_axi_rdaddrecc(NLW_U0_s_axi_rdaddrecc_UNCONNECTED[2:0]),
        .s_axi_rdata(NLW_U0_s_axi_rdata_UNCONNECTED[15:0]),
        .s_axi_rid(NLW_U0_s_axi_rid_UNCONNECTED[3:0]),
        .s_axi_rlast(NLW_U0_s_axi_rlast_UNCONNECTED),
        .s_axi_rready(1'b0),
        .s_axi_rresp(NLW_U0_s_axi_rresp_UNCONNECTED[1:0]),
        .s_axi_rvalid(NLW_U0_s_axi_rvalid_UNCONNECTED),
        .s_axi_sbiterr(NLW_U0_s_axi_sbiterr_UNCONNECTED),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wlast(1'b0),
        .s_axi_wready(NLW_U0_s_axi_wready_UNCONNECTED),
        .s_axi_wstrb(1'b0),
        .s_axi_wvalid(1'b0),
        .sbiterr(NLW_U0_sbiterr_UNCONNECTED),
        .shutdown(1'b0),
        .sleep(1'b0),
        .wea(wea),
        .web(1'b0));
endmodule

module fifo_n_m
   (\clk_rstn_intrf\.rst ,
    S,
    Q,
    \mem_reg[2][15]_0 ,
    DI,
    \mem_reg[0][14]_0 ,
    \mem_reg[0][14]_1 ,
    \mem_reg[1][6]_0 ,
    \mem_reg[1][15]_0 ,
    \mem_reg[1][6]_1 ,
    \mem_reg[1][14]_0 ,
    \mem_reg[1][14]_1 ,
    \mem_reg[0][6]_0 ,
    \mem_reg[0][6]_1 ,
    \mem_reg[0][14]_2 ,
    \mem_reg[0][14]_3 ,
    \mem_reg[1][0]_0 ,
    data_av_sync,
    CLK,
    D);
  output \clk_rstn_intrf\.rst ;
  output [3:0]S;
  output [15:0]Q;
  output [15:0]\mem_reg[2][15]_0 ;
  output [3:0]DI;
  output [3:0]\mem_reg[0][14]_0 ;
  output [3:0]\mem_reg[0][14]_1 ;
  output [3:0]\mem_reg[1][6]_0 ;
  output [15:0]\mem_reg[1][15]_0 ;
  output [3:0]\mem_reg[1][6]_1 ;
  output [3:0]\mem_reg[1][14]_0 ;
  output [3:0]\mem_reg[1][14]_1 ;
  output [3:0]\mem_reg[0][6]_0 ;
  output [3:0]\mem_reg[0][6]_1 ;
  output [3:0]\mem_reg[0][14]_2 ;
  output [3:0]\mem_reg[0][14]_3 ;
  input \mem_reg[1][0]_0 ;
  input data_av_sync;
  input CLK;
  input [15:0]D;

  wire CLK;
  wire [15:0]D;
  wire [3:0]DI;
  wire [15:0]Q;
  wire [3:0]S;
  wire \clk_rstn_intrf\.rst ;
  wire data_av_sync;
  wire mem;
  wire \mem[1][15]_i_1_n_1 ;
  wire \mem[2][15]_i_1_n_1 ;
  wire [3:0]\mem_reg[0][14]_0 ;
  wire [3:0]\mem_reg[0][14]_1 ;
  wire [3:0]\mem_reg[0][14]_2 ;
  wire [3:0]\mem_reg[0][14]_3 ;
  wire [3:0]\mem_reg[0][6]_0 ;
  wire [3:0]\mem_reg[0][6]_1 ;
  wire \mem_reg[1][0]_0 ;
  wire [3:0]\mem_reg[1][14]_0 ;
  wire [3:0]\mem_reg[1][14]_1 ;
  wire [15:0]\mem_reg[1][15]_0 ;
  wire [3:0]\mem_reg[1][6]_0 ;
  wire [3:0]\mem_reg[1][6]_1 ;
  wire [15:0]\mem_reg[2][15]_0 ;
  wire [1:0]write_index;
  wire \write_index[0]_i_1_n_1 ;
  wire \write_index[1]_i_1_n_1 ;

  LUT1 #(
    .INIT(2'h1)) 
    \FSM_sequential_fsm_state[1]_i_2 
       (.I0(\mem_reg[1][0]_0 ),
        .O(\clk_rstn_intrf\.rst ));
  LUT4 #(
    .INIT(16'h2F02)) 
    data_o1__15_carry__0_i_1
       (.I0(Q[14]),
        .I1(\mem_reg[1][15]_0 [14]),
        .I2(\mem_reg[1][15]_0 [15]),
        .I3(Q[15]),
        .O(\mem_reg[0][14]_3 [3]));
  LUT4 #(
    .INIT(16'h2F02)) 
    data_o1__15_carry__0_i_2
       (.I0(Q[12]),
        .I1(\mem_reg[1][15]_0 [12]),
        .I2(\mem_reg[1][15]_0 [13]),
        .I3(Q[13]),
        .O(\mem_reg[0][14]_3 [2]));
  LUT4 #(
    .INIT(16'h2F02)) 
    data_o1__15_carry__0_i_3
       (.I0(Q[10]),
        .I1(\mem_reg[1][15]_0 [10]),
        .I2(\mem_reg[1][15]_0 [11]),
        .I3(Q[11]),
        .O(\mem_reg[0][14]_3 [1]));
  LUT4 #(
    .INIT(16'h2F02)) 
    data_o1__15_carry__0_i_4
       (.I0(Q[8]),
        .I1(\mem_reg[1][15]_0 [8]),
        .I2(\mem_reg[1][15]_0 [9]),
        .I3(Q[9]),
        .O(\mem_reg[0][14]_3 [0]));
  LUT4 #(
    .INIT(16'h9009)) 
    data_o1__15_carry__0_i_5
       (.I0(Q[14]),
        .I1(\mem_reg[1][15]_0 [14]),
        .I2(Q[15]),
        .I3(\mem_reg[1][15]_0 [15]),
        .O(\mem_reg[0][14]_2 [3]));
  LUT4 #(
    .INIT(16'h9009)) 
    data_o1__15_carry__0_i_6
       (.I0(Q[12]),
        .I1(\mem_reg[1][15]_0 [12]),
        .I2(Q[13]),
        .I3(\mem_reg[1][15]_0 [13]),
        .O(\mem_reg[0][14]_2 [2]));
  LUT4 #(
    .INIT(16'h9009)) 
    data_o1__15_carry__0_i_7
       (.I0(Q[10]),
        .I1(\mem_reg[1][15]_0 [10]),
        .I2(Q[11]),
        .I3(\mem_reg[1][15]_0 [11]),
        .O(\mem_reg[0][14]_2 [1]));
  LUT4 #(
    .INIT(16'h9009)) 
    data_o1__15_carry__0_i_8
       (.I0(Q[8]),
        .I1(\mem_reg[1][15]_0 [8]),
        .I2(Q[9]),
        .I3(\mem_reg[1][15]_0 [9]),
        .O(\mem_reg[0][14]_2 [0]));
  LUT4 #(
    .INIT(16'h2F02)) 
    data_o1__15_carry_i_1
       (.I0(Q[6]),
        .I1(\mem_reg[1][15]_0 [6]),
        .I2(\mem_reg[1][15]_0 [7]),
        .I3(Q[7]),
        .O(\mem_reg[0][6]_1 [3]));
  LUT4 #(
    .INIT(16'h2F02)) 
    data_o1__15_carry_i_2
       (.I0(Q[4]),
        .I1(\mem_reg[1][15]_0 [4]),
        .I2(\mem_reg[1][15]_0 [5]),
        .I3(Q[5]),
        .O(\mem_reg[0][6]_1 [2]));
  LUT4 #(
    .INIT(16'h2F02)) 
    data_o1__15_carry_i_3
       (.I0(Q[2]),
        .I1(\mem_reg[1][15]_0 [2]),
        .I2(\mem_reg[1][15]_0 [3]),
        .I3(Q[3]),
        .O(\mem_reg[0][6]_1 [1]));
  LUT4 #(
    .INIT(16'h2F02)) 
    data_o1__15_carry_i_4
       (.I0(Q[0]),
        .I1(\mem_reg[1][15]_0 [0]),
        .I2(\mem_reg[1][15]_0 [1]),
        .I3(Q[1]),
        .O(\mem_reg[0][6]_1 [0]));
  LUT4 #(
    .INIT(16'h9009)) 
    data_o1__15_carry_i_5
       (.I0(Q[6]),
        .I1(\mem_reg[1][15]_0 [6]),
        .I2(Q[7]),
        .I3(\mem_reg[1][15]_0 [7]),
        .O(\mem_reg[0][6]_0 [3]));
  LUT4 #(
    .INIT(16'h9009)) 
    data_o1__15_carry_i_6
       (.I0(Q[4]),
        .I1(\mem_reg[1][15]_0 [4]),
        .I2(Q[5]),
        .I3(\mem_reg[1][15]_0 [5]),
        .O(\mem_reg[0][6]_0 [2]));
  LUT4 #(
    .INIT(16'h9009)) 
    data_o1__15_carry_i_7
       (.I0(Q[2]),
        .I1(\mem_reg[1][15]_0 [2]),
        .I2(Q[3]),
        .I3(\mem_reg[1][15]_0 [3]),
        .O(\mem_reg[0][6]_0 [1]));
  LUT4 #(
    .INIT(16'h9009)) 
    data_o1__15_carry_i_8
       (.I0(Q[0]),
        .I1(\mem_reg[1][15]_0 [0]),
        .I2(Q[1]),
        .I3(\mem_reg[1][15]_0 [1]),
        .O(\mem_reg[0][6]_0 [0]));
  LUT4 #(
    .INIT(16'h2F02)) 
    data_o1__7_carry__0_i_1
       (.I0(\mem_reg[1][15]_0 [14]),
        .I1(\mem_reg[2][15]_0 [14]),
        .I2(\mem_reg[2][15]_0 [15]),
        .I3(\mem_reg[1][15]_0 [15]),
        .O(\mem_reg[1][14]_1 [3]));
  LUT4 #(
    .INIT(16'h2F02)) 
    data_o1__7_carry__0_i_2
       (.I0(\mem_reg[1][15]_0 [12]),
        .I1(\mem_reg[2][15]_0 [12]),
        .I2(\mem_reg[2][15]_0 [13]),
        .I3(\mem_reg[1][15]_0 [13]),
        .O(\mem_reg[1][14]_1 [2]));
  LUT4 #(
    .INIT(16'h2F02)) 
    data_o1__7_carry__0_i_3
       (.I0(\mem_reg[1][15]_0 [10]),
        .I1(\mem_reg[2][15]_0 [10]),
        .I2(\mem_reg[2][15]_0 [11]),
        .I3(\mem_reg[1][15]_0 [11]),
        .O(\mem_reg[1][14]_1 [1]));
  LUT4 #(
    .INIT(16'h2F02)) 
    data_o1__7_carry__0_i_4
       (.I0(\mem_reg[1][15]_0 [8]),
        .I1(\mem_reg[2][15]_0 [8]),
        .I2(\mem_reg[2][15]_0 [9]),
        .I3(\mem_reg[1][15]_0 [9]),
        .O(\mem_reg[1][14]_1 [0]));
  LUT4 #(
    .INIT(16'h9009)) 
    data_o1__7_carry__0_i_5
       (.I0(\mem_reg[1][15]_0 [14]),
        .I1(\mem_reg[2][15]_0 [14]),
        .I2(\mem_reg[1][15]_0 [15]),
        .I3(\mem_reg[2][15]_0 [15]),
        .O(\mem_reg[1][14]_0 [3]));
  LUT4 #(
    .INIT(16'h9009)) 
    data_o1__7_carry__0_i_6
       (.I0(\mem_reg[1][15]_0 [12]),
        .I1(\mem_reg[2][15]_0 [12]),
        .I2(\mem_reg[1][15]_0 [13]),
        .I3(\mem_reg[2][15]_0 [13]),
        .O(\mem_reg[1][14]_0 [2]));
  LUT4 #(
    .INIT(16'h9009)) 
    data_o1__7_carry__0_i_7
       (.I0(\mem_reg[1][15]_0 [10]),
        .I1(\mem_reg[2][15]_0 [10]),
        .I2(\mem_reg[1][15]_0 [11]),
        .I3(\mem_reg[2][15]_0 [11]),
        .O(\mem_reg[1][14]_0 [1]));
  LUT4 #(
    .INIT(16'h9009)) 
    data_o1__7_carry__0_i_8
       (.I0(\mem_reg[1][15]_0 [8]),
        .I1(\mem_reg[2][15]_0 [8]),
        .I2(\mem_reg[1][15]_0 [9]),
        .I3(\mem_reg[2][15]_0 [9]),
        .O(\mem_reg[1][14]_0 [0]));
  LUT4 #(
    .INIT(16'h2F02)) 
    data_o1__7_carry_i_1
       (.I0(\mem_reg[1][15]_0 [6]),
        .I1(\mem_reg[2][15]_0 [6]),
        .I2(\mem_reg[2][15]_0 [7]),
        .I3(\mem_reg[1][15]_0 [7]),
        .O(\mem_reg[1][6]_1 [3]));
  LUT4 #(
    .INIT(16'h2F02)) 
    data_o1__7_carry_i_2
       (.I0(\mem_reg[1][15]_0 [4]),
        .I1(\mem_reg[2][15]_0 [4]),
        .I2(\mem_reg[2][15]_0 [5]),
        .I3(\mem_reg[1][15]_0 [5]),
        .O(\mem_reg[1][6]_1 [2]));
  LUT4 #(
    .INIT(16'h2F02)) 
    data_o1__7_carry_i_3
       (.I0(\mem_reg[1][15]_0 [2]),
        .I1(\mem_reg[2][15]_0 [2]),
        .I2(\mem_reg[2][15]_0 [3]),
        .I3(\mem_reg[1][15]_0 [3]),
        .O(\mem_reg[1][6]_1 [1]));
  LUT4 #(
    .INIT(16'h2F02)) 
    data_o1__7_carry_i_4
       (.I0(\mem_reg[1][15]_0 [0]),
        .I1(\mem_reg[2][15]_0 [0]),
        .I2(\mem_reg[2][15]_0 [1]),
        .I3(\mem_reg[1][15]_0 [1]),
        .O(\mem_reg[1][6]_1 [0]));
  LUT4 #(
    .INIT(16'h9009)) 
    data_o1__7_carry_i_5
       (.I0(\mem_reg[1][15]_0 [6]),
        .I1(\mem_reg[2][15]_0 [6]),
        .I2(\mem_reg[1][15]_0 [7]),
        .I3(\mem_reg[2][15]_0 [7]),
        .O(\mem_reg[1][6]_0 [3]));
  LUT4 #(
    .INIT(16'h9009)) 
    data_o1__7_carry_i_6
       (.I0(\mem_reg[1][15]_0 [4]),
        .I1(\mem_reg[2][15]_0 [4]),
        .I2(\mem_reg[1][15]_0 [5]),
        .I3(\mem_reg[2][15]_0 [5]),
        .O(\mem_reg[1][6]_0 [2]));
  LUT4 #(
    .INIT(16'h9009)) 
    data_o1__7_carry_i_7
       (.I0(\mem_reg[1][15]_0 [2]),
        .I1(\mem_reg[2][15]_0 [2]),
        .I2(\mem_reg[1][15]_0 [3]),
        .I3(\mem_reg[2][15]_0 [3]),
        .O(\mem_reg[1][6]_0 [1]));
  LUT4 #(
    .INIT(16'h9009)) 
    data_o1__7_carry_i_8
       (.I0(\mem_reg[1][15]_0 [0]),
        .I1(\mem_reg[2][15]_0 [0]),
        .I2(\mem_reg[1][15]_0 [1]),
        .I3(\mem_reg[2][15]_0 [1]),
        .O(\mem_reg[1][6]_0 [0]));
  LUT4 #(
    .INIT(16'h2F02)) 
    data_o1_carry__0_i_1
       (.I0(Q[14]),
        .I1(\mem_reg[2][15]_0 [14]),
        .I2(\mem_reg[2][15]_0 [15]),
        .I3(Q[15]),
        .O(\mem_reg[0][14]_1 [3]));
  LUT4 #(
    .INIT(16'h2F02)) 
    data_o1_carry__0_i_2
       (.I0(Q[12]),
        .I1(\mem_reg[2][15]_0 [12]),
        .I2(\mem_reg[2][15]_0 [13]),
        .I3(Q[13]),
        .O(\mem_reg[0][14]_1 [2]));
  LUT4 #(
    .INIT(16'h2F02)) 
    data_o1_carry__0_i_3
       (.I0(Q[10]),
        .I1(\mem_reg[2][15]_0 [10]),
        .I2(\mem_reg[2][15]_0 [11]),
        .I3(Q[11]),
        .O(\mem_reg[0][14]_1 [1]));
  LUT4 #(
    .INIT(16'h2F02)) 
    data_o1_carry__0_i_4
       (.I0(Q[8]),
        .I1(\mem_reg[2][15]_0 [8]),
        .I2(\mem_reg[2][15]_0 [9]),
        .I3(Q[9]),
        .O(\mem_reg[0][14]_1 [0]));
  LUT4 #(
    .INIT(16'h9009)) 
    data_o1_carry__0_i_5
       (.I0(Q[14]),
        .I1(\mem_reg[2][15]_0 [14]),
        .I2(Q[15]),
        .I3(\mem_reg[2][15]_0 [15]),
        .O(\mem_reg[0][14]_0 [3]));
  LUT4 #(
    .INIT(16'h9009)) 
    data_o1_carry__0_i_6
       (.I0(Q[12]),
        .I1(\mem_reg[2][15]_0 [12]),
        .I2(Q[13]),
        .I3(\mem_reg[2][15]_0 [13]),
        .O(\mem_reg[0][14]_0 [2]));
  LUT4 #(
    .INIT(16'h9009)) 
    data_o1_carry__0_i_7
       (.I0(Q[10]),
        .I1(\mem_reg[2][15]_0 [10]),
        .I2(Q[11]),
        .I3(\mem_reg[2][15]_0 [11]),
        .O(\mem_reg[0][14]_0 [1]));
  LUT4 #(
    .INIT(16'h9009)) 
    data_o1_carry__0_i_8
       (.I0(Q[8]),
        .I1(\mem_reg[2][15]_0 [8]),
        .I2(Q[9]),
        .I3(\mem_reg[2][15]_0 [9]),
        .O(\mem_reg[0][14]_0 [0]));
  LUT4 #(
    .INIT(16'h2F02)) 
    data_o1_carry_i_1
       (.I0(Q[6]),
        .I1(\mem_reg[2][15]_0 [6]),
        .I2(\mem_reg[2][15]_0 [7]),
        .I3(Q[7]),
        .O(DI[3]));
  LUT4 #(
    .INIT(16'h2F02)) 
    data_o1_carry_i_2
       (.I0(Q[4]),
        .I1(\mem_reg[2][15]_0 [4]),
        .I2(\mem_reg[2][15]_0 [5]),
        .I3(Q[5]),
        .O(DI[2]));
  LUT4 #(
    .INIT(16'h2F02)) 
    data_o1_carry_i_3
       (.I0(Q[2]),
        .I1(\mem_reg[2][15]_0 [2]),
        .I2(\mem_reg[2][15]_0 [3]),
        .I3(Q[3]),
        .O(DI[1]));
  LUT4 #(
    .INIT(16'h2F02)) 
    data_o1_carry_i_4
       (.I0(Q[0]),
        .I1(\mem_reg[2][15]_0 [0]),
        .I2(\mem_reg[2][15]_0 [1]),
        .I3(Q[1]),
        .O(DI[0]));
  LUT4 #(
    .INIT(16'h9009)) 
    data_o1_carry_i_5
       (.I0(Q[6]),
        .I1(\mem_reg[2][15]_0 [6]),
        .I2(Q[7]),
        .I3(\mem_reg[2][15]_0 [7]),
        .O(S[3]));
  LUT4 #(
    .INIT(16'h9009)) 
    data_o1_carry_i_6
       (.I0(Q[4]),
        .I1(\mem_reg[2][15]_0 [4]),
        .I2(Q[5]),
        .I3(\mem_reg[2][15]_0 [5]),
        .O(S[2]));
  LUT4 #(
    .INIT(16'h9009)) 
    data_o1_carry_i_7
       (.I0(Q[2]),
        .I1(\mem_reg[2][15]_0 [2]),
        .I2(Q[3]),
        .I3(\mem_reg[2][15]_0 [3]),
        .O(S[1]));
  LUT4 #(
    .INIT(16'h9009)) 
    data_o1_carry_i_8
       (.I0(Q[0]),
        .I1(\mem_reg[2][15]_0 [0]),
        .I2(Q[1]),
        .I3(\mem_reg[2][15]_0 [1]),
        .O(S[0]));
  LUT3 #(
    .INIT(8'h10)) 
    \mem[0][15]_i_1 
       (.I0(write_index[1]),
        .I1(write_index[0]),
        .I2(data_av_sync),
        .O(mem));
  LUT3 #(
    .INIT(8'h40)) 
    \mem[1][15]_i_1 
       (.I0(write_index[1]),
        .I1(write_index[0]),
        .I2(data_av_sync),
        .O(\mem[1][15]_i_1_n_1 ));
  LUT3 #(
    .INIT(8'h40)) 
    \mem[2][15]_i_1 
       (.I0(write_index[0]),
        .I1(write_index[1]),
        .I2(data_av_sync),
        .O(\mem[2][15]_i_1_n_1 ));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[0][0] 
       (.C(CLK),
        .CE(mem),
        .CLR(\clk_rstn_intrf\.rst ),
        .D(D[0]),
        .Q(Q[0]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[0][10] 
       (.C(CLK),
        .CE(mem),
        .CLR(\clk_rstn_intrf\.rst ),
        .D(D[10]),
        .Q(Q[10]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[0][11] 
       (.C(CLK),
        .CE(mem),
        .CLR(\clk_rstn_intrf\.rst ),
        .D(D[11]),
        .Q(Q[11]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[0][12] 
       (.C(CLK),
        .CE(mem),
        .CLR(\clk_rstn_intrf\.rst ),
        .D(D[12]),
        .Q(Q[12]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[0][13] 
       (.C(CLK),
        .CE(mem),
        .CLR(\clk_rstn_intrf\.rst ),
        .D(D[13]),
        .Q(Q[13]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[0][14] 
       (.C(CLK),
        .CE(mem),
        .CLR(\clk_rstn_intrf\.rst ),
        .D(D[14]),
        .Q(Q[14]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[0][15] 
       (.C(CLK),
        .CE(mem),
        .CLR(\clk_rstn_intrf\.rst ),
        .D(D[15]),
        .Q(Q[15]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[0][1] 
       (.C(CLK),
        .CE(mem),
        .CLR(\clk_rstn_intrf\.rst ),
        .D(D[1]),
        .Q(Q[1]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[0][2] 
       (.C(CLK),
        .CE(mem),
        .CLR(\clk_rstn_intrf\.rst ),
        .D(D[2]),
        .Q(Q[2]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[0][3] 
       (.C(CLK),
        .CE(mem),
        .CLR(\clk_rstn_intrf\.rst ),
        .D(D[3]),
        .Q(Q[3]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[0][4] 
       (.C(CLK),
        .CE(mem),
        .CLR(\clk_rstn_intrf\.rst ),
        .D(D[4]),
        .Q(Q[4]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[0][5] 
       (.C(CLK),
        .CE(mem),
        .CLR(\clk_rstn_intrf\.rst ),
        .D(D[5]),
        .Q(Q[5]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[0][6] 
       (.C(CLK),
        .CE(mem),
        .CLR(\clk_rstn_intrf\.rst ),
        .D(D[6]),
        .Q(Q[6]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[0][7] 
       (.C(CLK),
        .CE(mem),
        .CLR(\clk_rstn_intrf\.rst ),
        .D(D[7]),
        .Q(Q[7]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[0][8] 
       (.C(CLK),
        .CE(mem),
        .CLR(\clk_rstn_intrf\.rst ),
        .D(D[8]),
        .Q(Q[8]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[0][9] 
       (.C(CLK),
        .CE(mem),
        .CLR(\clk_rstn_intrf\.rst ),
        .D(D[9]),
        .Q(Q[9]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[1][0] 
       (.C(CLK),
        .CE(\mem[1][15]_i_1_n_1 ),
        .CLR(\clk_rstn_intrf\.rst ),
        .D(D[0]),
        .Q(\mem_reg[1][15]_0 [0]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[1][10] 
       (.C(CLK),
        .CE(\mem[1][15]_i_1_n_1 ),
        .CLR(\clk_rstn_intrf\.rst ),
        .D(D[10]),
        .Q(\mem_reg[1][15]_0 [10]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[1][11] 
       (.C(CLK),
        .CE(\mem[1][15]_i_1_n_1 ),
        .CLR(\clk_rstn_intrf\.rst ),
        .D(D[11]),
        .Q(\mem_reg[1][15]_0 [11]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[1][12] 
       (.C(CLK),
        .CE(\mem[1][15]_i_1_n_1 ),
        .CLR(\clk_rstn_intrf\.rst ),
        .D(D[12]),
        .Q(\mem_reg[1][15]_0 [12]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[1][13] 
       (.C(CLK),
        .CE(\mem[1][15]_i_1_n_1 ),
        .CLR(\clk_rstn_intrf\.rst ),
        .D(D[13]),
        .Q(\mem_reg[1][15]_0 [13]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[1][14] 
       (.C(CLK),
        .CE(\mem[1][15]_i_1_n_1 ),
        .CLR(\clk_rstn_intrf\.rst ),
        .D(D[14]),
        .Q(\mem_reg[1][15]_0 [14]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[1][15] 
       (.C(CLK),
        .CE(\mem[1][15]_i_1_n_1 ),
        .CLR(\clk_rstn_intrf\.rst ),
        .D(D[15]),
        .Q(\mem_reg[1][15]_0 [15]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[1][1] 
       (.C(CLK),
        .CE(\mem[1][15]_i_1_n_1 ),
        .CLR(\clk_rstn_intrf\.rst ),
        .D(D[1]),
        .Q(\mem_reg[1][15]_0 [1]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[1][2] 
       (.C(CLK),
        .CE(\mem[1][15]_i_1_n_1 ),
        .CLR(\clk_rstn_intrf\.rst ),
        .D(D[2]),
        .Q(\mem_reg[1][15]_0 [2]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[1][3] 
       (.C(CLK),
        .CE(\mem[1][15]_i_1_n_1 ),
        .CLR(\clk_rstn_intrf\.rst ),
        .D(D[3]),
        .Q(\mem_reg[1][15]_0 [3]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[1][4] 
       (.C(CLK),
        .CE(\mem[1][15]_i_1_n_1 ),
        .CLR(\clk_rstn_intrf\.rst ),
        .D(D[4]),
        .Q(\mem_reg[1][15]_0 [4]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[1][5] 
       (.C(CLK),
        .CE(\mem[1][15]_i_1_n_1 ),
        .CLR(\clk_rstn_intrf\.rst ),
        .D(D[5]),
        .Q(\mem_reg[1][15]_0 [5]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[1][6] 
       (.C(CLK),
        .CE(\mem[1][15]_i_1_n_1 ),
        .CLR(\clk_rstn_intrf\.rst ),
        .D(D[6]),
        .Q(\mem_reg[1][15]_0 [6]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[1][7] 
       (.C(CLK),
        .CE(\mem[1][15]_i_1_n_1 ),
        .CLR(\clk_rstn_intrf\.rst ),
        .D(D[7]),
        .Q(\mem_reg[1][15]_0 [7]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[1][8] 
       (.C(CLK),
        .CE(\mem[1][15]_i_1_n_1 ),
        .CLR(\clk_rstn_intrf\.rst ),
        .D(D[8]),
        .Q(\mem_reg[1][15]_0 [8]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[1][9] 
       (.C(CLK),
        .CE(\mem[1][15]_i_1_n_1 ),
        .CLR(\clk_rstn_intrf\.rst ),
        .D(D[9]),
        .Q(\mem_reg[1][15]_0 [9]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[2][0] 
       (.C(CLK),
        .CE(\mem[2][15]_i_1_n_1 ),
        .CLR(\clk_rstn_intrf\.rst ),
        .D(D[0]),
        .Q(\mem_reg[2][15]_0 [0]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[2][10] 
       (.C(CLK),
        .CE(\mem[2][15]_i_1_n_1 ),
        .CLR(\clk_rstn_intrf\.rst ),
        .D(D[10]),
        .Q(\mem_reg[2][15]_0 [10]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[2][11] 
       (.C(CLK),
        .CE(\mem[2][15]_i_1_n_1 ),
        .CLR(\clk_rstn_intrf\.rst ),
        .D(D[11]),
        .Q(\mem_reg[2][15]_0 [11]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[2][12] 
       (.C(CLK),
        .CE(\mem[2][15]_i_1_n_1 ),
        .CLR(\clk_rstn_intrf\.rst ),
        .D(D[12]),
        .Q(\mem_reg[2][15]_0 [12]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[2][13] 
       (.C(CLK),
        .CE(\mem[2][15]_i_1_n_1 ),
        .CLR(\clk_rstn_intrf\.rst ),
        .D(D[13]),
        .Q(\mem_reg[2][15]_0 [13]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[2][14] 
       (.C(CLK),
        .CE(\mem[2][15]_i_1_n_1 ),
        .CLR(\clk_rstn_intrf\.rst ),
        .D(D[14]),
        .Q(\mem_reg[2][15]_0 [14]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[2][15] 
       (.C(CLK),
        .CE(\mem[2][15]_i_1_n_1 ),
        .CLR(\clk_rstn_intrf\.rst ),
        .D(D[15]),
        .Q(\mem_reg[2][15]_0 [15]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[2][1] 
       (.C(CLK),
        .CE(\mem[2][15]_i_1_n_1 ),
        .CLR(\clk_rstn_intrf\.rst ),
        .D(D[1]),
        .Q(\mem_reg[2][15]_0 [1]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[2][2] 
       (.C(CLK),
        .CE(\mem[2][15]_i_1_n_1 ),
        .CLR(\clk_rstn_intrf\.rst ),
        .D(D[2]),
        .Q(\mem_reg[2][15]_0 [2]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[2][3] 
       (.C(CLK),
        .CE(\mem[2][15]_i_1_n_1 ),
        .CLR(\clk_rstn_intrf\.rst ),
        .D(D[3]),
        .Q(\mem_reg[2][15]_0 [3]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[2][4] 
       (.C(CLK),
        .CE(\mem[2][15]_i_1_n_1 ),
        .CLR(\clk_rstn_intrf\.rst ),
        .D(D[4]),
        .Q(\mem_reg[2][15]_0 [4]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[2][5] 
       (.C(CLK),
        .CE(\mem[2][15]_i_1_n_1 ),
        .CLR(\clk_rstn_intrf\.rst ),
        .D(D[5]),
        .Q(\mem_reg[2][15]_0 [5]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[2][6] 
       (.C(CLK),
        .CE(\mem[2][15]_i_1_n_1 ),
        .CLR(\clk_rstn_intrf\.rst ),
        .D(D[6]),
        .Q(\mem_reg[2][15]_0 [6]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[2][7] 
       (.C(CLK),
        .CE(\mem[2][15]_i_1_n_1 ),
        .CLR(\clk_rstn_intrf\.rst ),
        .D(D[7]),
        .Q(\mem_reg[2][15]_0 [7]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[2][8] 
       (.C(CLK),
        .CE(\mem[2][15]_i_1_n_1 ),
        .CLR(\clk_rstn_intrf\.rst ),
        .D(D[8]),
        .Q(\mem_reg[2][15]_0 [8]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[2][9] 
       (.C(CLK),
        .CE(\mem[2][15]_i_1_n_1 ),
        .CLR(\clk_rstn_intrf\.rst ),
        .D(D[9]),
        .Q(\mem_reg[2][15]_0 [9]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'hB4)) 
    \write_index[0]_i_1 
       (.I0(write_index[1]),
        .I1(data_av_sync),
        .I2(write_index[0]),
        .O(\write_index[0]_i_1_n_1 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'h38)) 
    \write_index[1]_i_1 
       (.I0(write_index[0]),
        .I1(data_av_sync),
        .I2(write_index[1]),
        .O(\write_index[1]_i_1_n_1 ));
  FDCE #(
    .INIT(1'b0)) 
    \write_index_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\clk_rstn_intrf\.rst ),
        .D(\write_index[0]_i_1_n_1 ),
        .Q(write_index[0]));
  FDCE #(
    .INIT(1'b0)) 
    \write_index_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\clk_rstn_intrf\.rst ),
        .D(\write_index[1]_i_1_n_1 ),
        .Q(write_index[1]));
endmodule

(* NotValidForBitStream *)
module filter_daq
   (\clk_rstn_intrf\.rst ,
    \clk_rstn_intrf\.clk ,
    data_i,
    data_av_ai,
    avg_o);
  input \clk_rstn_intrf\.rst ;
  input \clk_rstn_intrf\.clk ;
  input [15:0]data_i;
  input data_av_ai;
  output [15:0]avg_o;

  wire \FSM_sequential_fsm_state_reg[1]_i_3_n_1 ;
  wire [15:0]avg_o;
  wire [15:0]avg_o_OBUF;
  wire [2:0]bram_addra_i;
  wire [2:0]bram_addrb_i;
  wire [15:0]bram_doutb_i;
  wire \clk_rstn_intrf\.clk ;
  wire \clk_rstn_intrf\.rst ;
  wire control_fsm_i;
  wire control_fsm_wr;
  wire control_we_i;
  wire data_av_ai;
  wire data_av_ai_IBUF;
  wire data_av_sync;
  wire [15:0]data_i;
  wire [15:0]data_i_IBUF;
  wire median_filter_inst_n_10;
  wire median_filter_inst_n_11;
  wire median_filter_inst_n_12;
  wire median_filter_inst_n_13;
  wire median_filter_inst_n_14;
  wire median_filter_inst_n_15;
  wire median_filter_inst_n_16;
  wire median_filter_inst_n_17;
  wire median_filter_inst_n_18;
  wire median_filter_inst_n_2;
  wire median_filter_inst_n_3;
  wire median_filter_inst_n_4;
  wire median_filter_inst_n_5;
  wire median_filter_inst_n_6;
  wire median_filter_inst_n_7;
  wire median_filter_inst_n_8;
  wire median_filter_inst_n_9;
  wire n_0_112_BUFG;
  wire n_0_112_BUFG_inst_n_1;
  wire [15:0]wr_data;

  IBUF \FSM_sequential_fsm_state_reg[1]_i_3 
       (.I(\clk_rstn_intrf\.rst ),
        .O(\FSM_sequential_fsm_state_reg[1]_i_3_n_1 ));
  OBUF \avg_o_OBUF[0]_inst 
       (.I(avg_o_OBUF[0]),
        .O(avg_o[0]));
  OBUF \avg_o_OBUF[10]_inst 
       (.I(avg_o_OBUF[10]),
        .O(avg_o[10]));
  OBUF \avg_o_OBUF[11]_inst 
       (.I(avg_o_OBUF[11]),
        .O(avg_o[11]));
  OBUF \avg_o_OBUF[12]_inst 
       (.I(avg_o_OBUF[12]),
        .O(avg_o[12]));
  OBUF \avg_o_OBUF[13]_inst 
       (.I(avg_o_OBUF[13]),
        .O(avg_o[13]));
  OBUF \avg_o_OBUF[14]_inst 
       (.I(avg_o_OBUF[14]),
        .O(avg_o[14]));
  OBUF \avg_o_OBUF[15]_inst 
       (.I(avg_o_OBUF[15]),
        .O(avg_o[15]));
  OBUF \avg_o_OBUF[1]_inst 
       (.I(avg_o_OBUF[1]),
        .O(avg_o[1]));
  OBUF \avg_o_OBUF[2]_inst 
       (.I(avg_o_OBUF[2]),
        .O(avg_o[2]));
  OBUF \avg_o_OBUF[3]_inst 
       (.I(avg_o_OBUF[3]),
        .O(avg_o[3]));
  OBUF \avg_o_OBUF[4]_inst 
       (.I(avg_o_OBUF[4]),
        .O(avg_o[4]));
  OBUF \avg_o_OBUF[5]_inst 
       (.I(avg_o_OBUF[5]),
        .O(avg_o[5]));
  OBUF \avg_o_OBUF[6]_inst 
       (.I(avg_o_OBUF[6]),
        .O(avg_o[6]));
  OBUF \avg_o_OBUF[7]_inst 
       (.I(avg_o_OBUF[7]),
        .O(avg_o[7]));
  OBUF \avg_o_OBUF[8]_inst 
       (.I(avg_o_OBUF[8]),
        .O(avg_o[8]));
  OBUF \avg_o_OBUF[9]_inst 
       (.I(avg_o_OBUF[9]),
        .O(avg_o[9]));
  IBUF data_av_ai_IBUF_inst
       (.I(data_av_ai),
        .O(data_av_ai_IBUF));
  IBUF \data_i_IBUF[0]_inst 
       (.I(data_i[0]),
        .O(data_i_IBUF[0]));
  IBUF \data_i_IBUF[10]_inst 
       (.I(data_i[10]),
        .O(data_i_IBUF[10]));
  IBUF \data_i_IBUF[11]_inst 
       (.I(data_i[11]),
        .O(data_i_IBUF[11]));
  IBUF \data_i_IBUF[12]_inst 
       (.I(data_i[12]),
        .O(data_i_IBUF[12]));
  IBUF \data_i_IBUF[13]_inst 
       (.I(data_i[13]),
        .O(data_i_IBUF[13]));
  IBUF \data_i_IBUF[14]_inst 
       (.I(data_i[14]),
        .O(data_i_IBUF[14]));
  IBUF \data_i_IBUF[15]_inst 
       (.I(data_i[15]),
        .O(data_i_IBUF[15]));
  IBUF \data_i_IBUF[1]_inst 
       (.I(data_i[1]),
        .O(data_i_IBUF[1]));
  IBUF \data_i_IBUF[2]_inst 
       (.I(data_i[2]),
        .O(data_i_IBUF[2]));
  IBUF \data_i_IBUF[3]_inst 
       (.I(data_i[3]),
        .O(data_i_IBUF[3]));
  IBUF \data_i_IBUF[4]_inst 
       (.I(data_i[4]),
        .O(data_i_IBUF[4]));
  IBUF \data_i_IBUF[5]_inst 
       (.I(data_i[5]),
        .O(data_i_IBUF[5]));
  IBUF \data_i_IBUF[6]_inst 
       (.I(data_i[6]),
        .O(data_i_IBUF[6]));
  IBUF \data_i_IBUF[7]_inst 
       (.I(data_i[7]),
        .O(data_i_IBUF[7]));
  IBUF \data_i_IBUF[8]_inst 
       (.I(data_i[8]),
        .O(data_i_IBUF[8]));
  IBUF \data_i_IBUF[9]_inst 
       (.I(data_i[9]),
        .O(data_i_IBUF[9]));
  fsm_rd fsm_rd_inst
       (.CLK(n_0_112_BUFG),
        .Q(bram_addrb_i),
        .\avg_o_reg[15]_0 (avg_o_OBUF),
        .\avg_o_reg[15]_1 (median_filter_inst_n_2),
        .control_fsm_i(control_fsm_i),
        .doutb(bram_doutb_i));
  fsm_wr fsm_wr_inst
       (.CLK(n_0_112_BUFG),
        .D({median_filter_inst_n_3,median_filter_inst_n_4,median_filter_inst_n_5,median_filter_inst_n_6,median_filter_inst_n_7,median_filter_inst_n_8,median_filter_inst_n_9,median_filter_inst_n_10,median_filter_inst_n_11,median_filter_inst_n_12,median_filter_inst_n_13,median_filter_inst_n_14,median_filter_inst_n_15,median_filter_inst_n_16,median_filter_inst_n_17,median_filter_inst_n_18}),
        .Q(bram_addra_i),
        .control_fsm_i(control_fsm_i),
        .control_fsm_wr(control_fsm_wr),
        .control_we_o_reg_0(median_filter_inst_n_2),
        .\data_o_reg[15]_0 (wr_data),
        .\data_temp_reg[0]_0 (\FSM_sequential_fsm_state_reg[1]_i_3_n_1 ),
        .wea(control_we_i));
  median_filter_3 median_filter_inst
       (.CLK(n_0_112_BUFG),
        .D(data_i_IBUF),
        .\clk_rstn_intrf\.rst (median_filter_inst_n_2),
        .control_fsm_wr(control_fsm_wr),
        .data_av_sync(data_av_sync),
        .\mem_reg[0][15] ({median_filter_inst_n_3,median_filter_inst_n_4,median_filter_inst_n_5,median_filter_inst_n_6,median_filter_inst_n_7,median_filter_inst_n_8,median_filter_inst_n_9,median_filter_inst_n_10,median_filter_inst_n_11,median_filter_inst_n_12,median_filter_inst_n_13,median_filter_inst_n_14,median_filter_inst_n_15,median_filter_inst_n_16,median_filter_inst_n_17,median_filter_inst_n_18}),
        .\mem_reg[1][0] (\FSM_sequential_fsm_state_reg[1]_i_3_n_1 ));
  (* CHECK_LICENSE_TYPE = "blk_mem_gen_0,blk_mem_gen_v8_4_4,{}" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* x_core_info = "blk_mem_gen_v8_4_4,Vivado 2019.2" *) 
  blk_mem_gen_0 mem_inst
       (.addra(bram_addra_i),
        .addrb(bram_addrb_i),
        .clka(n_0_112_BUFG),
        .clkb(n_0_112_BUFG),
        .dina(wr_data),
        .doutb(bram_doutb_i),
        .wea(control_we_i));
  BUFG n_0_112_BUFG_inst
       (.I(n_0_112_BUFG_inst_n_1),
        .O(n_0_112_BUFG));
  IBUF n_0_112_BUFG_inst_i_1
       (.I(\clk_rstn_intrf\.clk ),
        .O(n_0_112_BUFG_inst_n_1));
  sync_stage sync_stage_inst
       (.CLK(n_0_112_BUFG),
        .data_av_ai_IBUF(data_av_ai_IBUF),
        .data_av_sync(data_av_sync),
        .\data_o_reg[0] (median_filter_inst_n_2));
endmodule

module fsm_rd
   (Q,
    \avg_o_reg[15]_0 ,
    CLK,
    \avg_o_reg[15]_1 ,
    control_fsm_i,
    doutb);
  output [2:0]Q;
  output [15:0]\avg_o_reg[15]_0 ;
  input CLK;
  input \avg_o_reg[15]_1 ;
  input control_fsm_i;
  input [15:0]doutb;

  wire CLK;
  wire \FSM_sequential_fsm_state[0]_i_1_n_1 ;
  wire \FSM_sequential_fsm_state[0]_i_2_n_1 ;
  wire \FSM_sequential_fsm_state[1]_i_1_n_1 ;
  wire \FSM_sequential_fsm_state[2]_i_1_n_1 ;
  wire [2:0]Q;
  wire \accumulator[0]_i_2_n_1 ;
  wire \accumulator[0]_i_3_n_1 ;
  wire \accumulator[0]_i_4_n_1 ;
  wire \accumulator[0]_i_5_n_1 ;
  wire \accumulator[12]_i_2_n_1 ;
  wire \accumulator[12]_i_3_n_1 ;
  wire \accumulator[12]_i_4_n_1 ;
  wire \accumulator[12]_i_5_n_1 ;
  wire \accumulator[4]_i_2_n_1 ;
  wire \accumulator[4]_i_3_n_1 ;
  wire \accumulator[4]_i_4_n_1 ;
  wire \accumulator[4]_i_5_n_1 ;
  wire \accumulator[8]_i_2_n_1 ;
  wire \accumulator[8]_i_3_n_1 ;
  wire \accumulator[8]_i_4_n_1 ;
  wire \accumulator[8]_i_5_n_1 ;
  wire \accumulator_reg[0]_i_1_n_1 ;
  wire \accumulator_reg[0]_i_1_n_2 ;
  wire \accumulator_reg[0]_i_1_n_3 ;
  wire \accumulator_reg[0]_i_1_n_4 ;
  wire \accumulator_reg[0]_i_1_n_5 ;
  wire \accumulator_reg[0]_i_1_n_6 ;
  wire \accumulator_reg[0]_i_1_n_7 ;
  wire \accumulator_reg[0]_i_1_n_8 ;
  wire \accumulator_reg[12]_i_1_n_1 ;
  wire \accumulator_reg[12]_i_1_n_2 ;
  wire \accumulator_reg[12]_i_1_n_3 ;
  wire \accumulator_reg[12]_i_1_n_4 ;
  wire \accumulator_reg[12]_i_1_n_5 ;
  wire \accumulator_reg[12]_i_1_n_6 ;
  wire \accumulator_reg[12]_i_1_n_7 ;
  wire \accumulator_reg[12]_i_1_n_8 ;
  wire \accumulator_reg[16]_i_1_n_3 ;
  wire \accumulator_reg[16]_i_1_n_4 ;
  wire \accumulator_reg[16]_i_1_n_6 ;
  wire \accumulator_reg[16]_i_1_n_7 ;
  wire \accumulator_reg[16]_i_1_n_8 ;
  wire \accumulator_reg[4]_i_1_n_1 ;
  wire \accumulator_reg[4]_i_1_n_2 ;
  wire \accumulator_reg[4]_i_1_n_3 ;
  wire \accumulator_reg[4]_i_1_n_4 ;
  wire \accumulator_reg[4]_i_1_n_5 ;
  wire \accumulator_reg[4]_i_1_n_6 ;
  wire \accumulator_reg[4]_i_1_n_7 ;
  wire \accumulator_reg[4]_i_1_n_8 ;
  wire \accumulator_reg[8]_i_1_n_1 ;
  wire \accumulator_reg[8]_i_1_n_2 ;
  wire \accumulator_reg[8]_i_1_n_3 ;
  wire \accumulator_reg[8]_i_1_n_4 ;
  wire \accumulator_reg[8]_i_1_n_5 ;
  wire \accumulator_reg[8]_i_1_n_6 ;
  wire \accumulator_reg[8]_i_1_n_7 ;
  wire \accumulator_reg[8]_i_1_n_8 ;
  wire \accumulator_reg_n_1_[0] ;
  wire \accumulator_reg_n_1_[1] ;
  wire \accumulator_reg_n_1_[2] ;
  wire \addr_o[0]_i_1__0_n_1 ;
  wire \addr_o[1]_i_1__0_n_1 ;
  wire \addr_o[2]_i_1_n_1 ;
  wire \addr_o[2]_i_2__0_n_1 ;
  wire \avg_o[15]_i_1_n_1 ;
  wire [15:0]\avg_o_reg[15]_0 ;
  wire \avg_o_reg[15]_1 ;
  wire control_fsm_i;
  wire [15:0]doutb;
  wire [2:0]fsm_state;
  wire [15:0]p_0_in;
  wire [3:2]\NLW_accumulator_reg[16]_i_1_CO_UNCONNECTED ;
  wire [3:3]\NLW_accumulator_reg[16]_i_1_O_UNCONNECTED ;

  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'hCD05)) 
    \FSM_sequential_fsm_state[0]_i_1 
       (.I0(\FSM_sequential_fsm_state[0]_i_2_n_1 ),
        .I1(fsm_state[2]),
        .I2(fsm_state[1]),
        .I3(fsm_state[0]),
        .O(\FSM_sequential_fsm_state[0]_i_1_n_1 ));
  LUT6 #(
    .INIT(64'hFF008000FF0080FF)) 
    \FSM_sequential_fsm_state[0]_i_2 
       (.I0(Q[2]),
        .I1(Q[0]),
        .I2(Q[1]),
        .I3(fsm_state[0]),
        .I4(fsm_state[2]),
        .I5(control_fsm_i),
        .O(\FSM_sequential_fsm_state[0]_i_2_n_1 ));
  LUT3 #(
    .INIT(8'h9C)) 
    \FSM_sequential_fsm_state[1]_i_1 
       (.I0(fsm_state[2]),
        .I1(fsm_state[1]),
        .I2(fsm_state[0]),
        .O(\FSM_sequential_fsm_state[1]_i_1_n_1 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    \FSM_sequential_fsm_state[2]_i_1 
       (.I0(fsm_state[2]),
        .I1(fsm_state[1]),
        .I2(fsm_state[0]),
        .O(\FSM_sequential_fsm_state[2]_i_1_n_1 ));
  (* FSM_ENCODED_STATES = "WAIT_MEM1:100,IDLE:000,READING:001,WAIT_MEM0:011,WAITING:010" *) 
  FDCE #(
    .INIT(1'b0)) 
    \FSM_sequential_fsm_state_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\avg_o_reg[15]_1 ),
        .D(\FSM_sequential_fsm_state[0]_i_1_n_1 ),
        .Q(fsm_state[0]));
  (* FSM_ENCODED_STATES = "WAIT_MEM1:100,IDLE:000,READING:001,WAIT_MEM0:011,WAITING:010" *) 
  FDCE #(
    .INIT(1'b0)) 
    \FSM_sequential_fsm_state_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\avg_o_reg[15]_1 ),
        .D(\FSM_sequential_fsm_state[1]_i_1_n_1 ),
        .Q(fsm_state[1]));
  (* FSM_ENCODED_STATES = "WAIT_MEM1:100,IDLE:000,READING:001,WAIT_MEM0:011,WAITING:010" *) 
  FDCE #(
    .INIT(1'b0)) 
    \FSM_sequential_fsm_state_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\avg_o_reg[15]_1 ),
        .D(\FSM_sequential_fsm_state[2]_i_1_n_1 ),
        .Q(fsm_state[2]));
  LUT2 #(
    .INIT(4'h6)) 
    \accumulator[0]_i_2 
       (.I0(doutb[3]),
        .I1(p_0_in[0]),
        .O(\accumulator[0]_i_2_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \accumulator[0]_i_3 
       (.I0(doutb[2]),
        .I1(\accumulator_reg_n_1_[2] ),
        .O(\accumulator[0]_i_3_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \accumulator[0]_i_4 
       (.I0(doutb[1]),
        .I1(\accumulator_reg_n_1_[1] ),
        .O(\accumulator[0]_i_4_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \accumulator[0]_i_5 
       (.I0(doutb[0]),
        .I1(\accumulator_reg_n_1_[0] ),
        .O(\accumulator[0]_i_5_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \accumulator[12]_i_2 
       (.I0(doutb[15]),
        .I1(p_0_in[12]),
        .O(\accumulator[12]_i_2_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \accumulator[12]_i_3 
       (.I0(doutb[14]),
        .I1(p_0_in[11]),
        .O(\accumulator[12]_i_3_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \accumulator[12]_i_4 
       (.I0(doutb[13]),
        .I1(p_0_in[10]),
        .O(\accumulator[12]_i_4_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \accumulator[12]_i_5 
       (.I0(doutb[12]),
        .I1(p_0_in[9]),
        .O(\accumulator[12]_i_5_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \accumulator[4]_i_2 
       (.I0(doutb[7]),
        .I1(p_0_in[4]),
        .O(\accumulator[4]_i_2_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \accumulator[4]_i_3 
       (.I0(doutb[6]),
        .I1(p_0_in[3]),
        .O(\accumulator[4]_i_3_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \accumulator[4]_i_4 
       (.I0(doutb[5]),
        .I1(p_0_in[2]),
        .O(\accumulator[4]_i_4_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \accumulator[4]_i_5 
       (.I0(doutb[4]),
        .I1(p_0_in[1]),
        .O(\accumulator[4]_i_5_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \accumulator[8]_i_2 
       (.I0(doutb[11]),
        .I1(p_0_in[8]),
        .O(\accumulator[8]_i_2_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \accumulator[8]_i_3 
       (.I0(doutb[10]),
        .I1(p_0_in[7]),
        .O(\accumulator[8]_i_3_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \accumulator[8]_i_4 
       (.I0(doutb[9]),
        .I1(p_0_in[6]),
        .O(\accumulator[8]_i_4_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \accumulator[8]_i_5 
       (.I0(doutb[8]),
        .I1(p_0_in[5]),
        .O(\accumulator[8]_i_5_n_1 ));
  FDCE #(
    .INIT(1'b0)) 
    \accumulator_reg[0] 
       (.C(CLK),
        .CE(\addr_o[2]_i_1_n_1 ),
        .CLR(\avg_o_reg[15]_1 ),
        .D(\accumulator_reg[0]_i_1_n_8 ),
        .Q(\accumulator_reg_n_1_[0] ));
  CARRY4 \accumulator_reg[0]_i_1 
       (.CI(1'b0),
        .CO({\accumulator_reg[0]_i_1_n_1 ,\accumulator_reg[0]_i_1_n_2 ,\accumulator_reg[0]_i_1_n_3 ,\accumulator_reg[0]_i_1_n_4 }),
        .CYINIT(1'b0),
        .DI(doutb[3:0]),
        .O({\accumulator_reg[0]_i_1_n_5 ,\accumulator_reg[0]_i_1_n_6 ,\accumulator_reg[0]_i_1_n_7 ,\accumulator_reg[0]_i_1_n_8 }),
        .S({\accumulator[0]_i_2_n_1 ,\accumulator[0]_i_3_n_1 ,\accumulator[0]_i_4_n_1 ,\accumulator[0]_i_5_n_1 }));
  FDCE #(
    .INIT(1'b0)) 
    \accumulator_reg[10] 
       (.C(CLK),
        .CE(\addr_o[2]_i_1_n_1 ),
        .CLR(\avg_o_reg[15]_1 ),
        .D(\accumulator_reg[8]_i_1_n_6 ),
        .Q(p_0_in[7]));
  FDCE #(
    .INIT(1'b0)) 
    \accumulator_reg[11] 
       (.C(CLK),
        .CE(\addr_o[2]_i_1_n_1 ),
        .CLR(\avg_o_reg[15]_1 ),
        .D(\accumulator_reg[8]_i_1_n_5 ),
        .Q(p_0_in[8]));
  FDCE #(
    .INIT(1'b0)) 
    \accumulator_reg[12] 
       (.C(CLK),
        .CE(\addr_o[2]_i_1_n_1 ),
        .CLR(\avg_o_reg[15]_1 ),
        .D(\accumulator_reg[12]_i_1_n_8 ),
        .Q(p_0_in[9]));
  CARRY4 \accumulator_reg[12]_i_1 
       (.CI(\accumulator_reg[8]_i_1_n_1 ),
        .CO({\accumulator_reg[12]_i_1_n_1 ,\accumulator_reg[12]_i_1_n_2 ,\accumulator_reg[12]_i_1_n_3 ,\accumulator_reg[12]_i_1_n_4 }),
        .CYINIT(1'b0),
        .DI(doutb[15:12]),
        .O({\accumulator_reg[12]_i_1_n_5 ,\accumulator_reg[12]_i_1_n_6 ,\accumulator_reg[12]_i_1_n_7 ,\accumulator_reg[12]_i_1_n_8 }),
        .S({\accumulator[12]_i_2_n_1 ,\accumulator[12]_i_3_n_1 ,\accumulator[12]_i_4_n_1 ,\accumulator[12]_i_5_n_1 }));
  FDCE #(
    .INIT(1'b0)) 
    \accumulator_reg[13] 
       (.C(CLK),
        .CE(\addr_o[2]_i_1_n_1 ),
        .CLR(\avg_o_reg[15]_1 ),
        .D(\accumulator_reg[12]_i_1_n_7 ),
        .Q(p_0_in[10]));
  FDCE #(
    .INIT(1'b0)) 
    \accumulator_reg[14] 
       (.C(CLK),
        .CE(\addr_o[2]_i_1_n_1 ),
        .CLR(\avg_o_reg[15]_1 ),
        .D(\accumulator_reg[12]_i_1_n_6 ),
        .Q(p_0_in[11]));
  FDCE #(
    .INIT(1'b0)) 
    \accumulator_reg[15] 
       (.C(CLK),
        .CE(\addr_o[2]_i_1_n_1 ),
        .CLR(\avg_o_reg[15]_1 ),
        .D(\accumulator_reg[12]_i_1_n_5 ),
        .Q(p_0_in[12]));
  FDCE #(
    .INIT(1'b0)) 
    \accumulator_reg[16] 
       (.C(CLK),
        .CE(\addr_o[2]_i_1_n_1 ),
        .CLR(\avg_o_reg[15]_1 ),
        .D(\accumulator_reg[16]_i_1_n_8 ),
        .Q(p_0_in[13]));
  CARRY4 \accumulator_reg[16]_i_1 
       (.CI(\accumulator_reg[12]_i_1_n_1 ),
        .CO({\NLW_accumulator_reg[16]_i_1_CO_UNCONNECTED [3:2],\accumulator_reg[16]_i_1_n_3 ,\accumulator_reg[16]_i_1_n_4 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_accumulator_reg[16]_i_1_O_UNCONNECTED [3],\accumulator_reg[16]_i_1_n_6 ,\accumulator_reg[16]_i_1_n_7 ,\accumulator_reg[16]_i_1_n_8 }),
        .S({1'b0,p_0_in[15:13]}));
  FDCE #(
    .INIT(1'b0)) 
    \accumulator_reg[17] 
       (.C(CLK),
        .CE(\addr_o[2]_i_1_n_1 ),
        .CLR(\avg_o_reg[15]_1 ),
        .D(\accumulator_reg[16]_i_1_n_7 ),
        .Q(p_0_in[14]));
  FDCE #(
    .INIT(1'b0)) 
    \accumulator_reg[18] 
       (.C(CLK),
        .CE(\addr_o[2]_i_1_n_1 ),
        .CLR(\avg_o_reg[15]_1 ),
        .D(\accumulator_reg[16]_i_1_n_6 ),
        .Q(p_0_in[15]));
  FDCE #(
    .INIT(1'b0)) 
    \accumulator_reg[1] 
       (.C(CLK),
        .CE(\addr_o[2]_i_1_n_1 ),
        .CLR(\avg_o_reg[15]_1 ),
        .D(\accumulator_reg[0]_i_1_n_7 ),
        .Q(\accumulator_reg_n_1_[1] ));
  FDCE #(
    .INIT(1'b0)) 
    \accumulator_reg[2] 
       (.C(CLK),
        .CE(\addr_o[2]_i_1_n_1 ),
        .CLR(\avg_o_reg[15]_1 ),
        .D(\accumulator_reg[0]_i_1_n_6 ),
        .Q(\accumulator_reg_n_1_[2] ));
  FDCE #(
    .INIT(1'b0)) 
    \accumulator_reg[3] 
       (.C(CLK),
        .CE(\addr_o[2]_i_1_n_1 ),
        .CLR(\avg_o_reg[15]_1 ),
        .D(\accumulator_reg[0]_i_1_n_5 ),
        .Q(p_0_in[0]));
  FDCE #(
    .INIT(1'b0)) 
    \accumulator_reg[4] 
       (.C(CLK),
        .CE(\addr_o[2]_i_1_n_1 ),
        .CLR(\avg_o_reg[15]_1 ),
        .D(\accumulator_reg[4]_i_1_n_8 ),
        .Q(p_0_in[1]));
  CARRY4 \accumulator_reg[4]_i_1 
       (.CI(\accumulator_reg[0]_i_1_n_1 ),
        .CO({\accumulator_reg[4]_i_1_n_1 ,\accumulator_reg[4]_i_1_n_2 ,\accumulator_reg[4]_i_1_n_3 ,\accumulator_reg[4]_i_1_n_4 }),
        .CYINIT(1'b0),
        .DI(doutb[7:4]),
        .O({\accumulator_reg[4]_i_1_n_5 ,\accumulator_reg[4]_i_1_n_6 ,\accumulator_reg[4]_i_1_n_7 ,\accumulator_reg[4]_i_1_n_8 }),
        .S({\accumulator[4]_i_2_n_1 ,\accumulator[4]_i_3_n_1 ,\accumulator[4]_i_4_n_1 ,\accumulator[4]_i_5_n_1 }));
  FDCE #(
    .INIT(1'b0)) 
    \accumulator_reg[5] 
       (.C(CLK),
        .CE(\addr_o[2]_i_1_n_1 ),
        .CLR(\avg_o_reg[15]_1 ),
        .D(\accumulator_reg[4]_i_1_n_7 ),
        .Q(p_0_in[2]));
  FDCE #(
    .INIT(1'b0)) 
    \accumulator_reg[6] 
       (.C(CLK),
        .CE(\addr_o[2]_i_1_n_1 ),
        .CLR(\avg_o_reg[15]_1 ),
        .D(\accumulator_reg[4]_i_1_n_6 ),
        .Q(p_0_in[3]));
  FDCE #(
    .INIT(1'b0)) 
    \accumulator_reg[7] 
       (.C(CLK),
        .CE(\addr_o[2]_i_1_n_1 ),
        .CLR(\avg_o_reg[15]_1 ),
        .D(\accumulator_reg[4]_i_1_n_5 ),
        .Q(p_0_in[4]));
  FDCE #(
    .INIT(1'b0)) 
    \accumulator_reg[8] 
       (.C(CLK),
        .CE(\addr_o[2]_i_1_n_1 ),
        .CLR(\avg_o_reg[15]_1 ),
        .D(\accumulator_reg[8]_i_1_n_8 ),
        .Q(p_0_in[5]));
  CARRY4 \accumulator_reg[8]_i_1 
       (.CI(\accumulator_reg[4]_i_1_n_1 ),
        .CO({\accumulator_reg[8]_i_1_n_1 ,\accumulator_reg[8]_i_1_n_2 ,\accumulator_reg[8]_i_1_n_3 ,\accumulator_reg[8]_i_1_n_4 }),
        .CYINIT(1'b0),
        .DI(doutb[11:8]),
        .O({\accumulator_reg[8]_i_1_n_5 ,\accumulator_reg[8]_i_1_n_6 ,\accumulator_reg[8]_i_1_n_7 ,\accumulator_reg[8]_i_1_n_8 }),
        .S({\accumulator[8]_i_2_n_1 ,\accumulator[8]_i_3_n_1 ,\accumulator[8]_i_4_n_1 ,\accumulator[8]_i_5_n_1 }));
  FDCE #(
    .INIT(1'b0)) 
    \accumulator_reg[9] 
       (.C(CLK),
        .CE(\addr_o[2]_i_1_n_1 ),
        .CLR(\avg_o_reg[15]_1 ),
        .D(\accumulator_reg[8]_i_1_n_7 ),
        .Q(p_0_in[6]));
  LUT1 #(
    .INIT(2'h1)) 
    \addr_o[0]_i_1__0 
       (.I0(Q[0]),
        .O(\addr_o[0]_i_1__0_n_1 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \addr_o[1]_i_1__0 
       (.I0(Q[0]),
        .I1(Q[1]),
        .O(\addr_o[1]_i_1__0_n_1 ));
  LUT3 #(
    .INIT(8'h02)) 
    \addr_o[2]_i_1 
       (.I0(fsm_state[0]),
        .I1(fsm_state[2]),
        .I2(fsm_state[1]),
        .O(\addr_o[2]_i_1_n_1 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \addr_o[2]_i_2__0 
       (.I0(Q[0]),
        .I1(Q[1]),
        .I2(Q[2]),
        .O(\addr_o[2]_i_2__0_n_1 ));
  FDCE #(
    .INIT(1'b0)) 
    \addr_o_reg[0] 
       (.C(CLK),
        .CE(\addr_o[2]_i_1_n_1 ),
        .CLR(\avg_o_reg[15]_1 ),
        .D(\addr_o[0]_i_1__0_n_1 ),
        .Q(Q[0]));
  FDCE #(
    .INIT(1'b0)) 
    \addr_o_reg[1] 
       (.C(CLK),
        .CE(\addr_o[2]_i_1_n_1 ),
        .CLR(\avg_o_reg[15]_1 ),
        .D(\addr_o[1]_i_1__0_n_1 ),
        .Q(Q[1]));
  FDCE #(
    .INIT(1'b0)) 
    \addr_o_reg[2] 
       (.C(CLK),
        .CE(\addr_o[2]_i_1_n_1 ),
        .CLR(\avg_o_reg[15]_1 ),
        .D(\addr_o[2]_i_2__0_n_1 ),
        .Q(Q[2]));
  LUT3 #(
    .INIT(8'h02)) 
    \avg_o[15]_i_1 
       (.I0(fsm_state[1]),
        .I1(fsm_state[2]),
        .I2(fsm_state[0]),
        .O(\avg_o[15]_i_1_n_1 ));
  FDCE #(
    .INIT(1'b0)) 
    \avg_o_reg[0] 
       (.C(CLK),
        .CE(\avg_o[15]_i_1_n_1 ),
        .CLR(\avg_o_reg[15]_1 ),
        .D(p_0_in[0]),
        .Q(\avg_o_reg[15]_0 [0]));
  FDCE #(
    .INIT(1'b0)) 
    \avg_o_reg[10] 
       (.C(CLK),
        .CE(\avg_o[15]_i_1_n_1 ),
        .CLR(\avg_o_reg[15]_1 ),
        .D(p_0_in[10]),
        .Q(\avg_o_reg[15]_0 [10]));
  FDCE #(
    .INIT(1'b0)) 
    \avg_o_reg[11] 
       (.C(CLK),
        .CE(\avg_o[15]_i_1_n_1 ),
        .CLR(\avg_o_reg[15]_1 ),
        .D(p_0_in[11]),
        .Q(\avg_o_reg[15]_0 [11]));
  FDCE #(
    .INIT(1'b0)) 
    \avg_o_reg[12] 
       (.C(CLK),
        .CE(\avg_o[15]_i_1_n_1 ),
        .CLR(\avg_o_reg[15]_1 ),
        .D(p_0_in[12]),
        .Q(\avg_o_reg[15]_0 [12]));
  FDCE #(
    .INIT(1'b0)) 
    \avg_o_reg[13] 
       (.C(CLK),
        .CE(\avg_o[15]_i_1_n_1 ),
        .CLR(\avg_o_reg[15]_1 ),
        .D(p_0_in[13]),
        .Q(\avg_o_reg[15]_0 [13]));
  FDCE #(
    .INIT(1'b0)) 
    \avg_o_reg[14] 
       (.C(CLK),
        .CE(\avg_o[15]_i_1_n_1 ),
        .CLR(\avg_o_reg[15]_1 ),
        .D(p_0_in[14]),
        .Q(\avg_o_reg[15]_0 [14]));
  FDCE #(
    .INIT(1'b0)) 
    \avg_o_reg[15] 
       (.C(CLK),
        .CE(\avg_o[15]_i_1_n_1 ),
        .CLR(\avg_o_reg[15]_1 ),
        .D(p_0_in[15]),
        .Q(\avg_o_reg[15]_0 [15]));
  FDCE #(
    .INIT(1'b0)) 
    \avg_o_reg[1] 
       (.C(CLK),
        .CE(\avg_o[15]_i_1_n_1 ),
        .CLR(\avg_o_reg[15]_1 ),
        .D(p_0_in[1]),
        .Q(\avg_o_reg[15]_0 [1]));
  FDCE #(
    .INIT(1'b0)) 
    \avg_o_reg[2] 
       (.C(CLK),
        .CE(\avg_o[15]_i_1_n_1 ),
        .CLR(\avg_o_reg[15]_1 ),
        .D(p_0_in[2]),
        .Q(\avg_o_reg[15]_0 [2]));
  FDCE #(
    .INIT(1'b0)) 
    \avg_o_reg[3] 
       (.C(CLK),
        .CE(\avg_o[15]_i_1_n_1 ),
        .CLR(\avg_o_reg[15]_1 ),
        .D(p_0_in[3]),
        .Q(\avg_o_reg[15]_0 [3]));
  FDCE #(
    .INIT(1'b0)) 
    \avg_o_reg[4] 
       (.C(CLK),
        .CE(\avg_o[15]_i_1_n_1 ),
        .CLR(\avg_o_reg[15]_1 ),
        .D(p_0_in[4]),
        .Q(\avg_o_reg[15]_0 [4]));
  FDCE #(
    .INIT(1'b0)) 
    \avg_o_reg[5] 
       (.C(CLK),
        .CE(\avg_o[15]_i_1_n_1 ),
        .CLR(\avg_o_reg[15]_1 ),
        .D(p_0_in[5]),
        .Q(\avg_o_reg[15]_0 [5]));
  FDCE #(
    .INIT(1'b0)) 
    \avg_o_reg[6] 
       (.C(CLK),
        .CE(\avg_o[15]_i_1_n_1 ),
        .CLR(\avg_o_reg[15]_1 ),
        .D(p_0_in[6]),
        .Q(\avg_o_reg[15]_0 [6]));
  FDCE #(
    .INIT(1'b0)) 
    \avg_o_reg[7] 
       (.C(CLK),
        .CE(\avg_o[15]_i_1_n_1 ),
        .CLR(\avg_o_reg[15]_1 ),
        .D(p_0_in[7]),
        .Q(\avg_o_reg[15]_0 [7]));
  FDCE #(
    .INIT(1'b0)) 
    \avg_o_reg[8] 
       (.C(CLK),
        .CE(\avg_o[15]_i_1_n_1 ),
        .CLR(\avg_o_reg[15]_1 ),
        .D(p_0_in[8]),
        .Q(\avg_o_reg[15]_0 [8]));
  FDCE #(
    .INIT(1'b0)) 
    \avg_o_reg[9] 
       (.C(CLK),
        .CE(\avg_o[15]_i_1_n_1 ),
        .CLR(\avg_o_reg[15]_1 ),
        .D(p_0_in[9]),
        .Q(\avg_o_reg[15]_0 [9]));
endmodule

module fsm_wr
   (wea,
    control_fsm_i,
    Q,
    \data_o_reg[15]_0 ,
    CLK,
    control_we_o_reg_0,
    \data_temp_reg[0]_0 ,
    control_fsm_wr,
    D);
  output [0:0]wea;
  output control_fsm_i;
  output [2:0]Q;
  output [15:0]\data_o_reg[15]_0 ;
  input CLK;
  input control_we_o_reg_0;
  input \data_temp_reg[0]_0 ;
  input control_fsm_wr;
  input [15:0]D;

  wire CLK;
  wire [15:0]D;
  wire [2:0]Q;
  wire \addr_o[0]_i_1_n_1 ;
  wire \addr_o[1]_i_1_n_1 ;
  wire \addr_o[2]_i_1__0_n_1 ;
  wire \addr_o[2]_i_2_n_1 ;
  wire control_fsm_i;
  wire control_fsm_o_i_1_n_1;
  wire control_fsm_wr;
  wire control_we_o_i_1_n_1;
  wire control_we_o_reg_0;
  wire \data_o[15]_i_1_n_1 ;
  wire [15:0]\data_o_reg[15]_0 ;
  wire [15:0]data_temp;
  wire \data_temp[15]_i_1_n_1 ;
  wire \data_temp[15]_i_3_n_1 ;
  wire \data_temp_reg[0]_0 ;
  wire [1:0]fsm_state;
  wire [1:0]fsm_state__0;
  wire [0:0]wea;

  LUT6 #(
    .INIT(64'hFEACACACACACACAC)) 
    \FSM_sequential_fsm_state[0]_i_1__0 
       (.I0(fsm_state[1]),
        .I1(control_fsm_wr),
        .I2(fsm_state[0]),
        .I3(Q[1]),
        .I4(Q[0]),
        .I5(Q[2]),
        .O(fsm_state__0[0]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'hEAAAAAAA)) 
    \FSM_sequential_fsm_state[1]_i_1__0 
       (.I0(fsm_state[0]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(Q[2]),
        .I4(fsm_state[1]),
        .O(fsm_state__0[1]));
  (* FSM_ENCODED_STATES = "INCREMENT:10,WAITING:11,IDLE:00,WRITING:01" *) 
  FDCE #(
    .INIT(1'b0)) 
    \FSM_sequential_fsm_state_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(control_we_o_reg_0),
        .D(fsm_state__0[0]),
        .Q(fsm_state[0]));
  (* FSM_ENCODED_STATES = "INCREMENT:10,WAITING:11,IDLE:00,WRITING:01" *) 
  FDCE #(
    .INIT(1'b0)) 
    \FSM_sequential_fsm_state_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(control_we_o_reg_0),
        .D(fsm_state__0[1]),
        .Q(fsm_state[1]));
  LUT1 #(
    .INIT(2'h1)) 
    \addr_o[0]_i_1 
       (.I0(Q[0]),
        .O(\addr_o[0]_i_1_n_1 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \addr_o[1]_i_1 
       (.I0(Q[0]),
        .I1(Q[1]),
        .O(\addr_o[1]_i_1_n_1 ));
  LUT2 #(
    .INIT(4'h2)) 
    \addr_o[2]_i_1__0 
       (.I0(fsm_state[1]),
        .I1(fsm_state[0]),
        .O(\addr_o[2]_i_1__0_n_1 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \addr_o[2]_i_2 
       (.I0(Q[0]),
        .I1(Q[1]),
        .I2(Q[2]),
        .O(\addr_o[2]_i_2_n_1 ));
  FDCE #(
    .INIT(1'b0)) 
    \addr_o_reg[0] 
       (.C(CLK),
        .CE(\addr_o[2]_i_1__0_n_1 ),
        .CLR(control_we_o_reg_0),
        .D(\addr_o[0]_i_1_n_1 ),
        .Q(Q[0]));
  FDCE #(
    .INIT(1'b0)) 
    \addr_o_reg[1] 
       (.C(CLK),
        .CE(\addr_o[2]_i_1__0_n_1 ),
        .CLR(control_we_o_reg_0),
        .D(\addr_o[1]_i_1_n_1 ),
        .Q(Q[1]));
  FDCE #(
    .INIT(1'b0)) 
    \addr_o_reg[2] 
       (.C(CLK),
        .CE(\addr_o[2]_i_1__0_n_1 ),
        .CLR(control_we_o_reg_0),
        .D(\addr_o[2]_i_2_n_1 ),
        .Q(Q[2]));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'hF8)) 
    control_fsm_o_i_1
       (.I0(fsm_state[1]),
        .I1(fsm_state[0]),
        .I2(control_fsm_i),
        .O(control_fsm_o_i_1_n_1));
  FDCE #(
    .INIT(1'b0)) 
    control_fsm_o_reg
       (.C(CLK),
        .CE(1'b1),
        .CLR(control_we_o_reg_0),
        .D(control_fsm_o_i_1_n_1),
        .Q(control_fsm_i));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT2 #(
    .INIT(4'h2)) 
    control_we_o_i_1
       (.I0(fsm_state[0]),
        .I1(fsm_state[1]),
        .O(control_we_o_i_1_n_1));
  FDCE #(
    .INIT(1'b0)) 
    control_we_o_reg
       (.C(CLK),
        .CE(1'b1),
        .CLR(control_we_o_reg_0),
        .D(control_we_o_i_1_n_1),
        .Q(wea));
  LUT3 #(
    .INIT(8'h20)) 
    \data_o[15]_i_1 
       (.I0(\data_temp_reg[0]_0 ),
        .I1(fsm_state[1]),
        .I2(fsm_state[0]),
        .O(\data_o[15]_i_1_n_1 ));
  FDRE #(
    .INIT(1'b0)) 
    \data_o_reg[0] 
       (.C(CLK),
        .CE(\data_o[15]_i_1_n_1 ),
        .D(data_temp[0]),
        .Q(\data_o_reg[15]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_o_reg[10] 
       (.C(CLK),
        .CE(\data_o[15]_i_1_n_1 ),
        .D(data_temp[10]),
        .Q(\data_o_reg[15]_0 [10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_o_reg[11] 
       (.C(CLK),
        .CE(\data_o[15]_i_1_n_1 ),
        .D(data_temp[11]),
        .Q(\data_o_reg[15]_0 [11]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_o_reg[12] 
       (.C(CLK),
        .CE(\data_o[15]_i_1_n_1 ),
        .D(data_temp[12]),
        .Q(\data_o_reg[15]_0 [12]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_o_reg[13] 
       (.C(CLK),
        .CE(\data_o[15]_i_1_n_1 ),
        .D(data_temp[13]),
        .Q(\data_o_reg[15]_0 [13]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_o_reg[14] 
       (.C(CLK),
        .CE(\data_o[15]_i_1_n_1 ),
        .D(data_temp[14]),
        .Q(\data_o_reg[15]_0 [14]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_o_reg[15] 
       (.C(CLK),
        .CE(\data_o[15]_i_1_n_1 ),
        .D(data_temp[15]),
        .Q(\data_o_reg[15]_0 [15]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_o_reg[1] 
       (.C(CLK),
        .CE(\data_o[15]_i_1_n_1 ),
        .D(data_temp[1]),
        .Q(\data_o_reg[15]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_o_reg[2] 
       (.C(CLK),
        .CE(\data_o[15]_i_1_n_1 ),
        .D(data_temp[2]),
        .Q(\data_o_reg[15]_0 [2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_o_reg[3] 
       (.C(CLK),
        .CE(\data_o[15]_i_1_n_1 ),
        .D(data_temp[3]),
        .Q(\data_o_reg[15]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_o_reg[4] 
       (.C(CLK),
        .CE(\data_o[15]_i_1_n_1 ),
        .D(data_temp[4]),
        .Q(\data_o_reg[15]_0 [4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_o_reg[5] 
       (.C(CLK),
        .CE(\data_o[15]_i_1_n_1 ),
        .D(data_temp[5]),
        .Q(\data_o_reg[15]_0 [5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_o_reg[6] 
       (.C(CLK),
        .CE(\data_o[15]_i_1_n_1 ),
        .D(data_temp[6]),
        .Q(\data_o_reg[15]_0 [6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_o_reg[7] 
       (.C(CLK),
        .CE(\data_o[15]_i_1_n_1 ),
        .D(data_temp[7]),
        .Q(\data_o_reg[15]_0 [7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_o_reg[8] 
       (.C(CLK),
        .CE(\data_o[15]_i_1_n_1 ),
        .D(data_temp[8]),
        .Q(\data_o_reg[15]_0 [8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_o_reg[9] 
       (.C(CLK),
        .CE(\data_o[15]_i_1_n_1 ),
        .D(data_temp[9]),
        .Q(\data_o_reg[15]_0 [9]),
        .R(1'b0));
  LUT5 #(
    .INIT(32'h02220000)) 
    \data_temp[15]_i_1 
       (.I0(\data_temp_reg[0]_0 ),
        .I1(fsm_state[0]),
        .I2(\data_temp[15]_i_3_n_1 ),
        .I3(fsm_state[1]),
        .I4(control_fsm_wr),
        .O(\data_temp[15]_i_1_n_1 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \data_temp[15]_i_3 
       (.I0(Q[1]),
        .I1(Q[0]),
        .I2(Q[2]),
        .O(\data_temp[15]_i_3_n_1 ));
  FDRE #(
    .INIT(1'b0)) 
    \data_temp_reg[0] 
       (.C(CLK),
        .CE(\data_temp[15]_i_1_n_1 ),
        .D(D[0]),
        .Q(data_temp[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_temp_reg[10] 
       (.C(CLK),
        .CE(\data_temp[15]_i_1_n_1 ),
        .D(D[10]),
        .Q(data_temp[10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_temp_reg[11] 
       (.C(CLK),
        .CE(\data_temp[15]_i_1_n_1 ),
        .D(D[11]),
        .Q(data_temp[11]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_temp_reg[12] 
       (.C(CLK),
        .CE(\data_temp[15]_i_1_n_1 ),
        .D(D[12]),
        .Q(data_temp[12]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_temp_reg[13] 
       (.C(CLK),
        .CE(\data_temp[15]_i_1_n_1 ),
        .D(D[13]),
        .Q(data_temp[13]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_temp_reg[14] 
       (.C(CLK),
        .CE(\data_temp[15]_i_1_n_1 ),
        .D(D[14]),
        .Q(data_temp[14]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_temp_reg[15] 
       (.C(CLK),
        .CE(\data_temp[15]_i_1_n_1 ),
        .D(D[15]),
        .Q(data_temp[15]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_temp_reg[1] 
       (.C(CLK),
        .CE(\data_temp[15]_i_1_n_1 ),
        .D(D[1]),
        .Q(data_temp[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_temp_reg[2] 
       (.C(CLK),
        .CE(\data_temp[15]_i_1_n_1 ),
        .D(D[2]),
        .Q(data_temp[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_temp_reg[3] 
       (.C(CLK),
        .CE(\data_temp[15]_i_1_n_1 ),
        .D(D[3]),
        .Q(data_temp[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_temp_reg[4] 
       (.C(CLK),
        .CE(\data_temp[15]_i_1_n_1 ),
        .D(D[4]),
        .Q(data_temp[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_temp_reg[5] 
       (.C(CLK),
        .CE(\data_temp[15]_i_1_n_1 ),
        .D(D[5]),
        .Q(data_temp[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_temp_reg[6] 
       (.C(CLK),
        .CE(\data_temp[15]_i_1_n_1 ),
        .D(D[6]),
        .Q(data_temp[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_temp_reg[7] 
       (.C(CLK),
        .CE(\data_temp[15]_i_1_n_1 ),
        .D(D[7]),
        .Q(data_temp[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_temp_reg[8] 
       (.C(CLK),
        .CE(\data_temp[15]_i_1_n_1 ),
        .D(D[8]),
        .Q(data_temp[8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_temp_reg[9] 
       (.C(CLK),
        .CE(\data_temp[15]_i_1_n_1 ),
        .D(D[9]),
        .Q(data_temp[9]),
        .R(1'b0));
endmodule

module median_filter_3
   (control_fsm_wr,
    \clk_rstn_intrf\.rst ,
    \mem_reg[0][15] ,
    data_av_sync,
    CLK,
    \mem_reg[1][0] ,
    D);
  output control_fsm_wr;
  output \clk_rstn_intrf\.rst ;
  output [15:0]\mem_reg[0][15] ;
  input data_av_sync;
  input CLK;
  input \mem_reg[1][0] ;
  input [15:0]D;

  wire CLK;
  wire [15:0]D;
  wire \clk_rstn_intrf\.rst ;
  wire control_fsm_wr;
  wire data_av_sync;
  wire data_o1;
  wire data_o10_in;
  wire data_o11_in;
  wire data_o1__15_carry__0_n_2;
  wire data_o1__15_carry__0_n_3;
  wire data_o1__15_carry__0_n_4;
  wire data_o1__15_carry_n_1;
  wire data_o1__15_carry_n_2;
  wire data_o1__15_carry_n_3;
  wire data_o1__15_carry_n_4;
  wire data_o1__7_carry__0_n_2;
  wire data_o1__7_carry__0_n_3;
  wire data_o1__7_carry__0_n_4;
  wire data_o1__7_carry_n_1;
  wire data_o1__7_carry_n_2;
  wire data_o1__7_carry_n_3;
  wire data_o1__7_carry_n_4;
  wire data_o1_carry__0_n_2;
  wire data_o1_carry__0_n_3;
  wire data_o1_carry__0_n_4;
  wire data_o1_carry_n_1;
  wire data_o1_carry_n_2;
  wire data_o1_carry_n_3;
  wire data_o1_carry_n_4;
  wire fifo_8_3_inst_n_2;
  wire fifo_8_3_inst_n_3;
  wire fifo_8_3_inst_n_38;
  wire fifo_8_3_inst_n_39;
  wire fifo_8_3_inst_n_4;
  wire fifo_8_3_inst_n_40;
  wire fifo_8_3_inst_n_41;
  wire fifo_8_3_inst_n_42;
  wire fifo_8_3_inst_n_43;
  wire fifo_8_3_inst_n_44;
  wire fifo_8_3_inst_n_45;
  wire fifo_8_3_inst_n_46;
  wire fifo_8_3_inst_n_47;
  wire fifo_8_3_inst_n_48;
  wire fifo_8_3_inst_n_49;
  wire fifo_8_3_inst_n_5;
  wire fifo_8_3_inst_n_50;
  wire fifo_8_3_inst_n_51;
  wire fifo_8_3_inst_n_52;
  wire fifo_8_3_inst_n_53;
  wire fifo_8_3_inst_n_70;
  wire fifo_8_3_inst_n_71;
  wire fifo_8_3_inst_n_72;
  wire fifo_8_3_inst_n_73;
  wire fifo_8_3_inst_n_74;
  wire fifo_8_3_inst_n_75;
  wire fifo_8_3_inst_n_76;
  wire fifo_8_3_inst_n_77;
  wire fifo_8_3_inst_n_78;
  wire fifo_8_3_inst_n_79;
  wire fifo_8_3_inst_n_80;
  wire fifo_8_3_inst_n_81;
  wire fifo_8_3_inst_n_82;
  wire fifo_8_3_inst_n_83;
  wire fifo_8_3_inst_n_84;
  wire fifo_8_3_inst_n_85;
  wire fifo_8_3_inst_n_86;
  wire fifo_8_3_inst_n_87;
  wire fifo_8_3_inst_n_88;
  wire fifo_8_3_inst_n_89;
  wire fifo_8_3_inst_n_90;
  wire fifo_8_3_inst_n_91;
  wire fifo_8_3_inst_n_92;
  wire fifo_8_3_inst_n_93;
  wire fifo_8_3_inst_n_94;
  wire fifo_8_3_inst_n_95;
  wire fifo_8_3_inst_n_96;
  wire fifo_8_3_inst_n_97;
  wire [15:0]\mem_reg[0] ;
  wire [15:0]\mem_reg[0][15] ;
  wire [15:0]\mem_reg[1] ;
  wire \mem_reg[1]_0_sn_1 ;
  wire [15:0]\mem_reg[2] ;
  wire [3:0]NLW_data_o1__15_carry_O_UNCONNECTED;
  wire [3:0]NLW_data_o1__15_carry__0_O_UNCONNECTED;
  wire [3:0]NLW_data_o1__7_carry_O_UNCONNECTED;
  wire [3:0]NLW_data_o1__7_carry__0_O_UNCONNECTED;
  wire [3:0]NLW_data_o1_carry_O_UNCONNECTED;
  wire [3:0]NLW_data_o1_carry__0_O_UNCONNECTED;

  assign \mem_reg[1]_0_sn_1  = \mem_reg[1][0] ;
  FDRE #(
    .INIT(1'b0)) 
    control_o_reg
       (.C(CLK),
        .CE(1'b1),
        .D(data_av_sync),
        .Q(control_fsm_wr),
        .R(1'b0));
  CARRY4 data_o1__15_carry
       (.CI(1'b0),
        .CO({data_o1__15_carry_n_1,data_o1__15_carry_n_2,data_o1__15_carry_n_3,data_o1__15_carry_n_4}),
        .CYINIT(1'b0),
        .DI({fifo_8_3_inst_n_86,fifo_8_3_inst_n_87,fifo_8_3_inst_n_88,fifo_8_3_inst_n_89}),
        .O(NLW_data_o1__15_carry_O_UNCONNECTED[3:0]),
        .S({fifo_8_3_inst_n_82,fifo_8_3_inst_n_83,fifo_8_3_inst_n_84,fifo_8_3_inst_n_85}));
  CARRY4 data_o1__15_carry__0
       (.CI(data_o1__15_carry_n_1),
        .CO({data_o11_in,data_o1__15_carry__0_n_2,data_o1__15_carry__0_n_3,data_o1__15_carry__0_n_4}),
        .CYINIT(1'b0),
        .DI({fifo_8_3_inst_n_94,fifo_8_3_inst_n_95,fifo_8_3_inst_n_96,fifo_8_3_inst_n_97}),
        .O(NLW_data_o1__15_carry__0_O_UNCONNECTED[3:0]),
        .S({fifo_8_3_inst_n_90,fifo_8_3_inst_n_91,fifo_8_3_inst_n_92,fifo_8_3_inst_n_93}));
  CARRY4 data_o1__7_carry
       (.CI(1'b0),
        .CO({data_o1__7_carry_n_1,data_o1__7_carry_n_2,data_o1__7_carry_n_3,data_o1__7_carry_n_4}),
        .CYINIT(1'b0),
        .DI({fifo_8_3_inst_n_70,fifo_8_3_inst_n_71,fifo_8_3_inst_n_72,fifo_8_3_inst_n_73}),
        .O(NLW_data_o1__7_carry_O_UNCONNECTED[3:0]),
        .S({fifo_8_3_inst_n_50,fifo_8_3_inst_n_51,fifo_8_3_inst_n_52,fifo_8_3_inst_n_53}));
  CARRY4 data_o1__7_carry__0
       (.CI(data_o1__7_carry_n_1),
        .CO({data_o10_in,data_o1__7_carry__0_n_2,data_o1__7_carry__0_n_3,data_o1__7_carry__0_n_4}),
        .CYINIT(1'b0),
        .DI({fifo_8_3_inst_n_78,fifo_8_3_inst_n_79,fifo_8_3_inst_n_80,fifo_8_3_inst_n_81}),
        .O(NLW_data_o1__7_carry__0_O_UNCONNECTED[3:0]),
        .S({fifo_8_3_inst_n_74,fifo_8_3_inst_n_75,fifo_8_3_inst_n_76,fifo_8_3_inst_n_77}));
  CARRY4 data_o1_carry
       (.CI(1'b0),
        .CO({data_o1_carry_n_1,data_o1_carry_n_2,data_o1_carry_n_3,data_o1_carry_n_4}),
        .CYINIT(1'b0),
        .DI({fifo_8_3_inst_n_38,fifo_8_3_inst_n_39,fifo_8_3_inst_n_40,fifo_8_3_inst_n_41}),
        .O(NLW_data_o1_carry_O_UNCONNECTED[3:0]),
        .S({fifo_8_3_inst_n_2,fifo_8_3_inst_n_3,fifo_8_3_inst_n_4,fifo_8_3_inst_n_5}));
  CARRY4 data_o1_carry__0
       (.CI(data_o1_carry_n_1),
        .CO({data_o1,data_o1_carry__0_n_2,data_o1_carry__0_n_3,data_o1_carry__0_n_4}),
        .CYINIT(1'b0),
        .DI({fifo_8_3_inst_n_46,fifo_8_3_inst_n_47,fifo_8_3_inst_n_48,fifo_8_3_inst_n_49}),
        .O(NLW_data_o1_carry__0_O_UNCONNECTED[3:0]),
        .S({fifo_8_3_inst_n_42,fifo_8_3_inst_n_43,fifo_8_3_inst_n_44,fifo_8_3_inst_n_45}));
  LUT6 #(
    .INIT(64'hEFEAFD5D4540A808)) 
    \data_temp[0]_i_1 
       (.I0(data_o11_in),
        .I1(\mem_reg[0] [0]),
        .I2(data_o1),
        .I3(\mem_reg[2] [0]),
        .I4(data_o10_in),
        .I5(\mem_reg[1] [0]),
        .O(\mem_reg[0][15] [0]));
  LUT6 #(
    .INIT(64'hEFEAFD5D4540A808)) 
    \data_temp[10]_i_1 
       (.I0(data_o11_in),
        .I1(\mem_reg[0] [10]),
        .I2(data_o1),
        .I3(\mem_reg[2] [10]),
        .I4(data_o10_in),
        .I5(\mem_reg[1] [10]),
        .O(\mem_reg[0][15] [10]));
  LUT6 #(
    .INIT(64'hEFEAFD5D4540A808)) 
    \data_temp[11]_i_1 
       (.I0(data_o11_in),
        .I1(\mem_reg[0] [11]),
        .I2(data_o1),
        .I3(\mem_reg[2] [11]),
        .I4(data_o10_in),
        .I5(\mem_reg[1] [11]),
        .O(\mem_reg[0][15] [11]));
  LUT6 #(
    .INIT(64'hEFEAFD5D4540A808)) 
    \data_temp[12]_i_1 
       (.I0(data_o11_in),
        .I1(\mem_reg[0] [12]),
        .I2(data_o1),
        .I3(\mem_reg[2] [12]),
        .I4(data_o10_in),
        .I5(\mem_reg[1] [12]),
        .O(\mem_reg[0][15] [12]));
  LUT6 #(
    .INIT(64'hEFEAFD5D4540A808)) 
    \data_temp[13]_i_1 
       (.I0(data_o11_in),
        .I1(\mem_reg[0] [13]),
        .I2(data_o1),
        .I3(\mem_reg[2] [13]),
        .I4(data_o10_in),
        .I5(\mem_reg[1] [13]),
        .O(\mem_reg[0][15] [13]));
  LUT6 #(
    .INIT(64'hEFEAFD5D4540A808)) 
    \data_temp[14]_i_1 
       (.I0(data_o11_in),
        .I1(\mem_reg[0] [14]),
        .I2(data_o1),
        .I3(\mem_reg[2] [14]),
        .I4(data_o10_in),
        .I5(\mem_reg[1] [14]),
        .O(\mem_reg[0][15] [14]));
  LUT6 #(
    .INIT(64'hEFEAFD5D4540A808)) 
    \data_temp[15]_i_2 
       (.I0(data_o11_in),
        .I1(\mem_reg[0] [15]),
        .I2(data_o1),
        .I3(\mem_reg[2] [15]),
        .I4(data_o10_in),
        .I5(\mem_reg[1] [15]),
        .O(\mem_reg[0][15] [15]));
  LUT6 #(
    .INIT(64'hEFEAFD5D4540A808)) 
    \data_temp[1]_i_1 
       (.I0(data_o11_in),
        .I1(\mem_reg[0] [1]),
        .I2(data_o1),
        .I3(\mem_reg[2] [1]),
        .I4(data_o10_in),
        .I5(\mem_reg[1] [1]),
        .O(\mem_reg[0][15] [1]));
  LUT6 #(
    .INIT(64'hEFEAFD5D4540A808)) 
    \data_temp[2]_i_1 
       (.I0(data_o11_in),
        .I1(\mem_reg[0] [2]),
        .I2(data_o1),
        .I3(\mem_reg[2] [2]),
        .I4(data_o10_in),
        .I5(\mem_reg[1] [2]),
        .O(\mem_reg[0][15] [2]));
  LUT6 #(
    .INIT(64'hEFEAFD5D4540A808)) 
    \data_temp[3]_i_1 
       (.I0(data_o11_in),
        .I1(\mem_reg[0] [3]),
        .I2(data_o1),
        .I3(\mem_reg[2] [3]),
        .I4(data_o10_in),
        .I5(\mem_reg[1] [3]),
        .O(\mem_reg[0][15] [3]));
  LUT6 #(
    .INIT(64'hEFEAFD5D4540A808)) 
    \data_temp[4]_i_1 
       (.I0(data_o11_in),
        .I1(\mem_reg[0] [4]),
        .I2(data_o1),
        .I3(\mem_reg[2] [4]),
        .I4(data_o10_in),
        .I5(\mem_reg[1] [4]),
        .O(\mem_reg[0][15] [4]));
  LUT6 #(
    .INIT(64'hEFEAFD5D4540A808)) 
    \data_temp[5]_i_1 
       (.I0(data_o11_in),
        .I1(\mem_reg[0] [5]),
        .I2(data_o1),
        .I3(\mem_reg[2] [5]),
        .I4(data_o10_in),
        .I5(\mem_reg[1] [5]),
        .O(\mem_reg[0][15] [5]));
  LUT6 #(
    .INIT(64'hEFEAFD5D4540A808)) 
    \data_temp[6]_i_1 
       (.I0(data_o11_in),
        .I1(\mem_reg[0] [6]),
        .I2(data_o1),
        .I3(\mem_reg[2] [6]),
        .I4(data_o10_in),
        .I5(\mem_reg[1] [6]),
        .O(\mem_reg[0][15] [6]));
  LUT6 #(
    .INIT(64'hEFEAFD5D4540A808)) 
    \data_temp[7]_i_1 
       (.I0(data_o11_in),
        .I1(\mem_reg[0] [7]),
        .I2(data_o1),
        .I3(\mem_reg[2] [7]),
        .I4(data_o10_in),
        .I5(\mem_reg[1] [7]),
        .O(\mem_reg[0][15] [7]));
  LUT6 #(
    .INIT(64'hEFEAFD5D4540A808)) 
    \data_temp[8]_i_1 
       (.I0(data_o11_in),
        .I1(\mem_reg[0] [8]),
        .I2(data_o1),
        .I3(\mem_reg[2] [8]),
        .I4(data_o10_in),
        .I5(\mem_reg[1] [8]),
        .O(\mem_reg[0][15] [8]));
  LUT6 #(
    .INIT(64'hEFEAFD5D4540A808)) 
    \data_temp[9]_i_1 
       (.I0(data_o11_in),
        .I1(\mem_reg[0] [9]),
        .I2(data_o1),
        .I3(\mem_reg[2] [9]),
        .I4(data_o10_in),
        .I5(\mem_reg[1] [9]),
        .O(\mem_reg[0][15] [9]));
  fifo_n_m fifo_8_3_inst
       (.CLK(CLK),
        .D(D),
        .DI({fifo_8_3_inst_n_38,fifo_8_3_inst_n_39,fifo_8_3_inst_n_40,fifo_8_3_inst_n_41}),
        .Q(\mem_reg[0] ),
        .S({fifo_8_3_inst_n_2,fifo_8_3_inst_n_3,fifo_8_3_inst_n_4,fifo_8_3_inst_n_5}),
        .\clk_rstn_intrf\.rst (\clk_rstn_intrf\.rst ),
        .data_av_sync(data_av_sync),
        .\mem_reg[0][14]_0 ({fifo_8_3_inst_n_42,fifo_8_3_inst_n_43,fifo_8_3_inst_n_44,fifo_8_3_inst_n_45}),
        .\mem_reg[0][14]_1 ({fifo_8_3_inst_n_46,fifo_8_3_inst_n_47,fifo_8_3_inst_n_48,fifo_8_3_inst_n_49}),
        .\mem_reg[0][14]_2 ({fifo_8_3_inst_n_90,fifo_8_3_inst_n_91,fifo_8_3_inst_n_92,fifo_8_3_inst_n_93}),
        .\mem_reg[0][14]_3 ({fifo_8_3_inst_n_94,fifo_8_3_inst_n_95,fifo_8_3_inst_n_96,fifo_8_3_inst_n_97}),
        .\mem_reg[0][6]_0 ({fifo_8_3_inst_n_82,fifo_8_3_inst_n_83,fifo_8_3_inst_n_84,fifo_8_3_inst_n_85}),
        .\mem_reg[0][6]_1 ({fifo_8_3_inst_n_86,fifo_8_3_inst_n_87,fifo_8_3_inst_n_88,fifo_8_3_inst_n_89}),
        .\mem_reg[1][0]_0 (\mem_reg[1]_0_sn_1 ),
        .\mem_reg[1][14]_0 ({fifo_8_3_inst_n_74,fifo_8_3_inst_n_75,fifo_8_3_inst_n_76,fifo_8_3_inst_n_77}),
        .\mem_reg[1][14]_1 ({fifo_8_3_inst_n_78,fifo_8_3_inst_n_79,fifo_8_3_inst_n_80,fifo_8_3_inst_n_81}),
        .\mem_reg[1][15]_0 (\mem_reg[1] ),
        .\mem_reg[1][6]_0 ({fifo_8_3_inst_n_50,fifo_8_3_inst_n_51,fifo_8_3_inst_n_52,fifo_8_3_inst_n_53}),
        .\mem_reg[1][6]_1 ({fifo_8_3_inst_n_70,fifo_8_3_inst_n_71,fifo_8_3_inst_n_72,fifo_8_3_inst_n_73}),
        .\mem_reg[2][15]_0 (\mem_reg[2] ));
endmodule

module register_n
   (data_o,
    data_av_ai_IBUF,
    CLK,
    \data_o_reg[0]_0 );
  output data_o;
  input data_av_ai_IBUF;
  input CLK;
  input \data_o_reg[0]_0 ;

  wire CLK;
  wire data_av_ai_IBUF;
  wire data_o;
  wire \data_o_reg[0]_0 ;

  FDCE #(
    .INIT(1'b0)) 
    \data_o_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\data_o_reg[0]_0 ),
        .D(data_av_ai_IBUF),
        .Q(data_o));
endmodule

(* ORIG_REF_NAME = "register_n" *) 
module register_n_0
   (data_av_sync,
    data_o,
    CLK,
    \data_o_reg[0]_0 );
  output data_av_sync;
  input data_o;
  input CLK;
  input \data_o_reg[0]_0 ;

  wire CLK;
  wire data_av_sync;
  wire data_o;
  wire \data_o_reg[0]_0 ;

  FDCE #(
    .INIT(1'b0)) 
    \data_o_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(\data_o_reg[0]_0 ),
        .D(data_o),
        .Q(data_av_sync));
endmodule

module sync_stage
   (data_av_sync,
    data_av_ai_IBUF,
    CLK,
    \data_o_reg[0] );
  output data_av_sync;
  input data_av_ai_IBUF;
  input CLK;
  input \data_o_reg[0] ;

  wire CLK;
  wire data_av_ai_IBUF;
  wire data_av_sync;
  wire data_o;
  wire \data_o_reg[0] ;

  register_n sync_stage_0
       (.CLK(CLK),
        .data_av_ai_IBUF(data_av_ai_IBUF),
        .data_o(data_o),
        .\data_o_reg[0]_0 (\data_o_reg[0] ));
  register_n_0 sync_stage_1
       (.CLK(CLK),
        .data_av_sync(data_av_sync),
        .data_o(data_o),
        .\data_o_reg[0]_0 (\data_o_reg[0] ));
endmodule

module blk_mem_gen_generic_cstr
   (doutb,
    clka,
    wea,
    addrb,
    addra,
    dina);
  output [15:0]doutb;
  input clka;
  input [0:0]wea;
  input [2:0]addrb;
  input [2:0]addra;
  input [15:0]dina;

  wire [2:0]addra;
  wire [2:0]addrb;
  wire clka;
  wire [15:0]dina;
  wire [15:0]doutb;
  wire [0:0]wea;

  blk_mem_gen_prim_width \ramloop[0].ram.r 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .dina(dina),
        .doutb(doutb),
        .wea(wea));
endmodule

module blk_mem_gen_prim_width
   (doutb,
    clka,
    wea,
    addrb,
    addra,
    dina);
  output [15:0]doutb;
  input clka;
  input [0:0]wea;
  input [2:0]addrb;
  input [2:0]addra;
  input [15:0]dina;

  wire [2:0]addra;
  wire [2:0]addrb;
  wire clka;
  wire [15:0]dina;
  wire [15:0]doutb;
  wire [0:0]wea;

  blk_mem_gen_prim_wrapper \prim_noinit.ram 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .dina(dina),
        .doutb(doutb),
        .wea(wea));
endmodule

module blk_mem_gen_prim_wrapper
   (doutb,
    clka,
    wea,
    addrb,
    addra,
    dina);
  output [15:0]doutb;
  input clka;
  input [0:0]wea;
  input [2:0]addrb;
  input [2:0]addra;
  input [15:0]dina;

  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM18.ram_n_1 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM18.ram_n_10 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM18.ram_n_11 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM18.ram_n_12 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM18.ram_n_17 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM18.ram_n_18 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM18.ram_n_19 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM18.ram_n_2 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM18.ram_n_20 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM18.ram_n_25 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM18.ram_n_26 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM18.ram_n_27 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM18.ram_n_28 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM18.ram_n_3 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM18.ram_n_33 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM18.ram_n_34 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM18.ram_n_35 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM18.ram_n_36 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM18.ram_n_4 ;
  wire \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM18.ram_n_9 ;
  wire [2:0]addra;
  wire [2:0]addrb;
  wire clka;
  wire [15:0]dina;
  wire [15:0]doutb;
  wire [0:0]wea;

  (* box_type = "PRIMITIVE" *) 
  RAMB18E1 #(
    .DOA_REG(1),
    .DOB_REG(1),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_10(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_11(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_12(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_13(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_14(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_15(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_16(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_17(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_18(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_19(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_20(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_21(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_22(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_23(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_24(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_25(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_26(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_27(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_28(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_29(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_30(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_31(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_32(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_33(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_34(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_35(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_36(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_37(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_38(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_39(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .INIT_FILE("NONE"),
    .IS_CLKARDCLK_INVERTED(1'b0),
    .IS_CLKBWRCLK_INVERTED(1'b0),
    .IS_ENARDEN_INVERTED(1'b0),
    .IS_ENBWREN_INVERTED(1'b0),
    .IS_RSTRAMARSTRAM_INVERTED(1'b0),
    .IS_RSTRAMB_INVERTED(1'b0),
    .IS_RSTREGARSTREG_INVERTED(1'b0),
    .IS_RSTREGB_INVERTED(1'b0),
    .RAM_MODE("SDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(36),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("REGCE"),
    .RSTREG_PRIORITY_B("REGCE"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("READ_FIRST"),
    .WRITE_WIDTH_A(0),
    .WRITE_WIDTH_B(36)) 
    \DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM18.ram 
       (.ADDRARDADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,addrb,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ADDRBWRADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,addra,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CLKARDCLK(clka),
        .CLKBWRCLK(clka),
        .DIADI({1'b0,1'b0,1'b0,1'b0,dina[7:4],1'b0,1'b0,1'b0,1'b0,dina[3:0]}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,dina[15:12],1'b0,1'b0,1'b0,1'b0,dina[11:8]}),
        .DIPADIP({1'b0,1'b0}),
        .DIPBDIP({1'b0,1'b0}),
        .DOADO({\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM18.ram_n_1 ,\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM18.ram_n_2 ,\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM18.ram_n_3 ,\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM18.ram_n_4 ,doutb[7:4],\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM18.ram_n_9 ,\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM18.ram_n_10 ,\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM18.ram_n_11 ,\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM18.ram_n_12 ,doutb[3:0]}),
        .DOBDO({\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM18.ram_n_17 ,\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM18.ram_n_18 ,\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM18.ram_n_19 ,\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM18.ram_n_20 ,doutb[15:12],\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM18.ram_n_25 ,\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM18.ram_n_26 ,\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM18.ram_n_27 ,\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM18.ram_n_28 ,doutb[11:8]}),
        .DOPADOP({\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM18.ram_n_33 ,\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM18.ram_n_34 }),
        .DOPBDOP({\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM18.ram_n_35 ,\DEVICE_7SERIES.NO_BMM_INFO.SDP.WIDE_PRIM18.ram_n_36 }),
        .ENARDEN(1'b1),
        .ENBWREN(wea),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({1'b0,1'b0}),
        .WEBWE({1'b1,1'b1,1'b1,1'b1}));
endmodule

module blk_mem_gen_top
   (doutb,
    clka,
    wea,
    addrb,
    addra,
    dina);
  output [15:0]doutb;
  input clka;
  input [0:0]wea;
  input [2:0]addrb;
  input [2:0]addra;
  input [15:0]dina;

  wire [2:0]addra;
  wire [2:0]addrb;
  wire clka;
  wire [15:0]dina;
  wire [15:0]doutb;
  wire [0:0]wea;

  blk_mem_gen_generic_cstr \valid.cstr 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .dina(dina),
        .doutb(doutb),
        .wea(wea));
endmodule

(* C_ADDRA_WIDTH = "3" *) (* C_ADDRB_WIDTH = "3" *) (* C_ALGORITHM = "1" *) 
(* C_AXI_ID_WIDTH = "4" *) (* C_AXI_SLAVE_TYPE = "0" *) (* C_AXI_TYPE = "1" *) 
(* C_BYTE_SIZE = "9" *) (* C_COMMON_CLK = "1" *) (* C_COUNT_18K_BRAM = "1" *) 
(* C_COUNT_36K_BRAM = "0" *) (* C_CTRL_ECC_ALGO = "NONE" *) (* C_DEFAULT_DATA = "0" *) 
(* C_DISABLE_WARN_BHV_COLL = "0" *) (* C_DISABLE_WARN_BHV_RANGE = "0" *) (* C_ELABORATION_DIR = "./" *) 
(* C_ENABLE_32BIT_ADDRESS = "0" *) (* C_EN_DEEPSLEEP_PIN = "0" *) (* C_EN_ECC_PIPE = "0" *) 
(* C_EN_RDADDRA_CHG = "0" *) (* C_EN_RDADDRB_CHG = "0" *) (* C_EN_SAFETY_CKT = "0" *) 
(* C_EN_SHUTDOWN_PIN = "0" *) (* C_EN_SLEEP_PIN = "0" *) (* C_EST_POWER_SUMMARY = "Estimated Power for IP     :     3.01735 mW" *) 
(* C_FAMILY = "zynq" *) (* C_HAS_AXI_ID = "0" *) (* C_HAS_ENA = "0" *) 
(* C_HAS_ENB = "0" *) (* C_HAS_INJECTERR = "0" *) (* C_HAS_MEM_OUTPUT_REGS_A = "0" *) 
(* C_HAS_MEM_OUTPUT_REGS_B = "1" *) (* C_HAS_MUX_OUTPUT_REGS_A = "0" *) (* C_HAS_MUX_OUTPUT_REGS_B = "0" *) 
(* C_HAS_REGCEA = "0" *) (* C_HAS_REGCEB = "0" *) (* C_HAS_RSTA = "0" *) 
(* C_HAS_RSTB = "0" *) (* C_HAS_SOFTECC_INPUT_REGS_A = "0" *) (* C_HAS_SOFTECC_OUTPUT_REGS_B = "0" *) 
(* C_INITA_VAL = "0" *) (* C_INITB_VAL = "0" *) (* C_INIT_FILE = "blk_mem_gen_0.mem" *) 
(* C_INIT_FILE_NAME = "no_coe_file_loaded" *) (* C_INTERFACE_TYPE = "0" *) (* C_LOAD_INIT_FILE = "0" *) 
(* C_MEM_TYPE = "1" *) (* C_MUX_PIPELINE_STAGES = "0" *) (* C_PRIM_TYPE = "1" *) 
(* C_READ_DEPTH_A = "8" *) (* C_READ_DEPTH_B = "8" *) (* C_READ_LATENCY_A = "1" *) 
(* C_READ_LATENCY_B = "1" *) (* C_READ_WIDTH_A = "16" *) (* C_READ_WIDTH_B = "16" *) 
(* C_RSTRAM_A = "0" *) (* C_RSTRAM_B = "0" *) (* C_RST_PRIORITY_A = "CE" *) 
(* C_RST_PRIORITY_B = "CE" *) (* C_SIM_COLLISION_CHECK = "ALL" *) (* C_USE_BRAM_BLOCK = "0" *) 
(* C_USE_BYTE_WEA = "0" *) (* C_USE_BYTE_WEB = "0" *) (* C_USE_DEFAULT_DATA = "0" *) 
(* C_USE_ECC = "0" *) (* C_USE_SOFTECC = "0" *) (* C_USE_URAM = "0" *) 
(* C_WEA_WIDTH = "1" *) (* C_WEB_WIDTH = "1" *) (* C_WRITE_DEPTH_A = "8" *) 
(* C_WRITE_DEPTH_B = "8" *) (* C_WRITE_MODE_A = "NO_CHANGE" *) (* C_WRITE_MODE_B = "READ_FIRST" *) 
(* C_WRITE_WIDTH_A = "16" *) (* C_WRITE_WIDTH_B = "16" *) (* C_XDEVICEFAMILY = "zynq" *) 
(* downgradeipidentifiedwarnings = "yes" *) 
module blk_mem_gen_v8_4_4
   (clka,
    rsta,
    ena,
    regcea,
    wea,
    addra,
    dina,
    douta,
    clkb,
    rstb,
    enb,
    regceb,
    web,
    addrb,
    dinb,
    doutb,
    injectsbiterr,
    injectdbiterr,
    eccpipece,
    sbiterr,
    dbiterr,
    rdaddrecc,
    sleep,
    deepsleep,
    shutdown,
    rsta_busy,
    rstb_busy,
    s_aclk,
    s_aresetn,
    s_axi_awid,
    s_axi_awaddr,
    s_axi_awlen,
    s_axi_awsize,
    s_axi_awburst,
    s_axi_awvalid,
    s_axi_awready,
    s_axi_wdata,
    s_axi_wstrb,
    s_axi_wlast,
    s_axi_wvalid,
    s_axi_wready,
    s_axi_bid,
    s_axi_bresp,
    s_axi_bvalid,
    s_axi_bready,
    s_axi_arid,
    s_axi_araddr,
    s_axi_arlen,
    s_axi_arsize,
    s_axi_arburst,
    s_axi_arvalid,
    s_axi_arready,
    s_axi_rid,
    s_axi_rdata,
    s_axi_rresp,
    s_axi_rlast,
    s_axi_rvalid,
    s_axi_rready,
    s_axi_injectsbiterr,
    s_axi_injectdbiterr,
    s_axi_sbiterr,
    s_axi_dbiterr,
    s_axi_rdaddrecc);
  input clka;
  input rsta;
  input ena;
  input regcea;
  input [0:0]wea;
  input [2:0]addra;
  input [15:0]dina;
  output [15:0]douta;
  input clkb;
  input rstb;
  input enb;
  input regceb;
  input [0:0]web;
  input [2:0]addrb;
  input [15:0]dinb;
  output [15:0]doutb;
  input injectsbiterr;
  input injectdbiterr;
  input eccpipece;
  output sbiterr;
  output dbiterr;
  output [2:0]rdaddrecc;
  input sleep;
  input deepsleep;
  input shutdown;
  output rsta_busy;
  output rstb_busy;
  input s_aclk;
  input s_aresetn;
  input [3:0]s_axi_awid;
  input [31:0]s_axi_awaddr;
  input [7:0]s_axi_awlen;
  input [2:0]s_axi_awsize;
  input [1:0]s_axi_awburst;
  input s_axi_awvalid;
  output s_axi_awready;
  input [15:0]s_axi_wdata;
  input [0:0]s_axi_wstrb;
  input s_axi_wlast;
  input s_axi_wvalid;
  output s_axi_wready;
  output [3:0]s_axi_bid;
  output [1:0]s_axi_bresp;
  output s_axi_bvalid;
  input s_axi_bready;
  input [3:0]s_axi_arid;
  input [31:0]s_axi_araddr;
  input [7:0]s_axi_arlen;
  input [2:0]s_axi_arsize;
  input [1:0]s_axi_arburst;
  input s_axi_arvalid;
  output s_axi_arready;
  output [3:0]s_axi_rid;
  output [15:0]s_axi_rdata;
  output [1:0]s_axi_rresp;
  output s_axi_rlast;
  output s_axi_rvalid;
  input s_axi_rready;
  input s_axi_injectsbiterr;
  input s_axi_injectdbiterr;
  output s_axi_sbiterr;
  output s_axi_dbiterr;
  output [2:0]s_axi_rdaddrecc;

  wire \<const0> ;
  wire [2:0]addra;
  wire [2:0]addrb;
  wire clka;
  wire [15:0]dina;
  wire [15:0]doutb;
  wire [0:0]wea;

  assign dbiterr = \<const0> ;
  assign douta[15] = \<const0> ;
  assign douta[14] = \<const0> ;
  assign douta[13] = \<const0> ;
  assign douta[12] = \<const0> ;
  assign douta[11] = \<const0> ;
  assign douta[10] = \<const0> ;
  assign douta[9] = \<const0> ;
  assign douta[8] = \<const0> ;
  assign douta[7] = \<const0> ;
  assign douta[6] = \<const0> ;
  assign douta[5] = \<const0> ;
  assign douta[4] = \<const0> ;
  assign douta[3] = \<const0> ;
  assign douta[2] = \<const0> ;
  assign douta[1] = \<const0> ;
  assign douta[0] = \<const0> ;
  assign rdaddrecc[2] = \<const0> ;
  assign rdaddrecc[1] = \<const0> ;
  assign rdaddrecc[0] = \<const0> ;
  assign rsta_busy = \<const0> ;
  assign rstb_busy = \<const0> ;
  assign s_axi_arready = \<const0> ;
  assign s_axi_awready = \<const0> ;
  assign s_axi_bid[3] = \<const0> ;
  assign s_axi_bid[2] = \<const0> ;
  assign s_axi_bid[1] = \<const0> ;
  assign s_axi_bid[0] = \<const0> ;
  assign s_axi_bresp[1] = \<const0> ;
  assign s_axi_bresp[0] = \<const0> ;
  assign s_axi_bvalid = \<const0> ;
  assign s_axi_dbiterr = \<const0> ;
  assign s_axi_rdaddrecc[2] = \<const0> ;
  assign s_axi_rdaddrecc[1] = \<const0> ;
  assign s_axi_rdaddrecc[0] = \<const0> ;
  assign s_axi_rdata[15] = \<const0> ;
  assign s_axi_rdata[14] = \<const0> ;
  assign s_axi_rdata[13] = \<const0> ;
  assign s_axi_rdata[12] = \<const0> ;
  assign s_axi_rdata[11] = \<const0> ;
  assign s_axi_rdata[10] = \<const0> ;
  assign s_axi_rdata[9] = \<const0> ;
  assign s_axi_rdata[8] = \<const0> ;
  assign s_axi_rdata[7] = \<const0> ;
  assign s_axi_rdata[6] = \<const0> ;
  assign s_axi_rdata[5] = \<const0> ;
  assign s_axi_rdata[4] = \<const0> ;
  assign s_axi_rdata[3] = \<const0> ;
  assign s_axi_rdata[2] = \<const0> ;
  assign s_axi_rdata[1] = \<const0> ;
  assign s_axi_rdata[0] = \<const0> ;
  assign s_axi_rid[3] = \<const0> ;
  assign s_axi_rid[2] = \<const0> ;
  assign s_axi_rid[1] = \<const0> ;
  assign s_axi_rid[0] = \<const0> ;
  assign s_axi_rlast = \<const0> ;
  assign s_axi_rresp[1] = \<const0> ;
  assign s_axi_rresp[0] = \<const0> ;
  assign s_axi_rvalid = \<const0> ;
  assign s_axi_sbiterr = \<const0> ;
  assign s_axi_wready = \<const0> ;
  assign sbiterr = \<const0> ;
  GND GND
       (.G(\<const0> ));
  blk_mem_gen_v8_4_4_synth inst_blk_mem_gen
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .dina(dina),
        .doutb(doutb),
        .wea(wea));
endmodule

module blk_mem_gen_v8_4_4_synth
   (doutb,
    clka,
    wea,
    addrb,
    addra,
    dina);
  output [15:0]doutb;
  input clka;
  input [0:0]wea;
  input [2:0]addrb;
  input [2:0]addra;
  input [15:0]dina;

  wire [2:0]addra;
  wire [2:0]addrb;
  wire clka;
  wire [15:0]dina;
  wire [15:0]doutb;
  wire [0:0]wea;

  blk_mem_gen_top \gnbram.gnativebmg.native_blk_mem_gen 
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .dina(dina),
        .doutb(doutb),
        .wea(wea));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
