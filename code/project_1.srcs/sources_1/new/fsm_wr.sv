//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/31/2020 03:07:45 PM
// Design Name: 
// Module Name: fsm_wr
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description:
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module fsm_wr
  (
   clk_rst_if.slave clk_rstn_intrf,
   input logic control_i,
   input logic [15:0]  data_i,	// Median value from filter
   output logic [15:0]  data_o,	// Output value for BRAM
   output logic [2:0] addr_o,	// RAM Address to write to
   output logic control_we_o,	// Write enable for RAM
   output logic control_fsm_o	// Signal for fsm_rd to start
   );

   // Typedef for FSM states
   typedef enum logic[1:0] {IDLE, WRITING, INCREMENT, WAITING} state_t;
   state_t fsm_state;			// State storage

   logic [15:0] data_temp;		// Used to store data when signal from median
								// is received
   
   always_ff @(posedge clk_rstn_intrf.clk or negedge clk_rstn_intrf.rst)
	 begin : FSM
		if (~clk_rstn_intrf.rst) // On negedge of reset 
		  begin : RESET
			 fsm_state <= IDLE;
			 addr_o <= 3'd0;
			 control_we_o <= 1'b0;
			 control_fsm_o <= 1'b0;
			 addr_o <= 1'b0;
		  end : RESET
		else					// On posedge of CLK
		  begin : STATES
			 case(fsm_state)
			   IDLE:
				 begin
					control_we_o <= 1'b0; // Do not write to RAM
					if(control_i) // Signal from median filter
					  begin
						 data_temp <= data_i;
						 fsm_state <= WRITING;
					  end
					else
					  fsm_state <= IDLE;
				 end

			   WRITING:
				 begin
					data_o <= data_temp;  // 
					control_we_o <= 1'b1; // Write enable for BRAM
					if(addr_o >= 3'd7) // Filled up BRAM
					  begin
						 fsm_state <= WAITING;
					  end
					else
					  begin
						 fsm_state <= INCREMENT;
					  end
				 end

			   INCREMENT:
				 begin
					control_we_o <= 1'b0;
					addr_o <= addr_o + 1;
					if(addr_o >= 3'd7) // Filled up BRAM
					  begin
						 fsm_state <= WAITING;
					  end
					else
					  begin
					  	 if(~control_i)			  // No more data
					  	   begin
					  		  fsm_state <= IDLE;
					  	   end
						 else
						   begin
							  data_temp <= data_i;
					  		  fsm_state <= WRITING;					  
						   end
					  end					
				 end
			   
			   WAITING:			// Done writing 8 values
				 begin
					control_we_o <= 1'b0;
					control_fsm_o <= 1'b1;
					fsm_state <= WAITING;											
				 end
			 endcase
		  end : STATES
	 end : FSM


   // PROPERTIES AND ASSERTIONS
   
   // If data available and have not written all of RAM,
   // next address must be incremented by 1
   property addr_incr;
	  @(posedge clk_rstn_intrf.clk) disable iff (!clk_rstn_intrf.rst || addr_o >= 7)
		(control_i) |->##2 (addr_o == ($past(addr_o) + 1));
   endproperty // addr_incr
   
   addr_incr_assert: assert property (addr_incr);

   // On new data, next state must be WRITING
   property state_change_write;
	  @(posedge clk_rstn_intrf.clk) disable iff (!clk_rstn_intrf.rst || addr_o >= 7)
		(control_i) |-> (fsm_state == WRITING);
   endproperty // state_change_write

   state_change_write_assert: assert property (state_change_write);
   
   // After writing --> increment
   property state_change_increment;
	  @(posedge clk_rstn_intrf.clk) disable iff (addr_o >=7)
		(fsm_state == WRITING) |=> (fsm_state == INCREMENT);
   endproperty // state_change_increment
   
   state_change_increment_assert: assert property (state_change_increment);

   // After increment --> writing
   property state_change_writing;
	  @(posedge clk_rstn_intrf.clk) 
		(fsm_state == INCREMENT && control_i) |=> (fsm_state == WRITING);
   endproperty // state_change_increment
   
   state_change_writing_assert: assert property (state_change_writing);   
endmodule : fsm_wr // fsm_wr

