// `timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/24/2020 09:18:02 PM
// Design Name: 
// Module Name: register_n
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module register_n
  #(parameter WIDTH = 8)
   (
	input logic [WIDTH-1:0]  data_i,
	input logic 			 clk_i,
	input logic 			 rstn_i,
	input logic 			 enable_i,
	output logic [WIDTH-1:0] data_o
	);

   // SystemVerilog: always_ff - sequential behavior intent specification
   always_ff @(posedge clk_i, negedge rstn_i) 
	 begin
		if (!rstn_i)
		  data_o <= 0;
		else if (enable_i)
		  data_o <= data_i;
	 end

   // Concurrent Assertion
   property dataCheck;
	  @(posedge clk_i) disable iff (!rstn_i)
		// On the same cycle, out_o and previous cycle
		// data_i are the same.
		// Checks flipflop behavior.
		(enable_i) |=> ($past(data_i) == data_o);
   endproperty // dataCheck
   
   dataCheck_Assert: assert property (dataCheck);

endmodule:register_n
