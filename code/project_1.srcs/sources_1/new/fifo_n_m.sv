// `timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/25/2020 11:59:47 AM
// Design Name: 
// Module Name: fifo_n_m
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// Simple configurable FIFO
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// Adapted version of http://www.asic-world.com/examples/systemverilog/syn_fifo.html
// Initially intended for reading a single datum from the FIFO, like "popping" it,
// ultimately removing it with each read. However we need to read all three
// items simultaneously each time
//////////////////////////////////////////////////////////////////////////////////
module fifo_n_m
  #(DATA_WIDTH=16)
   (
	input logic [DATA_WIDTH-1:0]  data_i,
	clk_rst_if.slave clk_rstn_intrf, // CLK and RST interface in
	input logic 				  write_en_i, // Write enable signal
	output logic [DATA_WIDTH-1:0] data0_o , // Data out
	output logic [DATA_WIDTH-1:0] data1_o , // Data out
	output logic [DATA_WIDTH-1:0] data2_o , // Data out
	output logic 				  full_o // FIFO full
    );
   
   // Internal variables
   logic 						  empty_o;
   logic [DATA_WIDTH-1:0] 		  mem [0:2]; // Internal memory for FIFO
   logic [1:0] 					  items_count = 0; // Total number of elements stored
   logic [1:0] 					  write_index = 0; // Last write address
   // logic [1:0] 					  read_index = 0;  // Last read adress

   // Items count checker
   always_comb
	 begin : COUNT_ITEMS
		if(items_count == 3)
		  begin
			 full_o = 1'b1;
		  end
		else
		  begin
			 full_o = 1'b0;
		  end
		if(items_count == 0)
		  begin
			 empty_o = 1'b1;
		  end
		else
		  begin
			 empty_o = 1'b0;
		  end
	 end : COUNT_ITEMS

   always_ff @ (posedge clk_rstn_intrf.clk, negedge clk_rstn_intrf.rst)
	 begin
		if(!clk_rstn_intrf.rst)
		  begin : RESET			// Reset signal received
			 for (int i = 0; i < 3; i++)
			   begin : FIFO_CLEAR
				  mem[i] = 0;
			   end : FIFO_CLEAR
			 write_index = 0;
			 items_count = 0;
		  end : RESET
		else
		  begin
			 if(write_en_i)		// No need to check if full
			   begin : WRITE_DATA
				  mem[write_index] = data_i;
				  write_index = (write_index + 1) % 3;
				  if(!full_o)
					items_count += 1; // Once reached full, items count stays at 3
			   end : WRITE_DATA
		  end
	 end // always_ff @ (posedge clk_rstn_intrf.clk, negedge clk_rstn_intrf.rst)

   always_comb
	 // begin
		// if(!empty_o)
		  begin : OUTPUT_DATA
			 data0_o = mem[0];
			 data1_o = mem[1];
			 data2_o = mem[2];
		  end : OUTPUT_DATA
	 // end
   
   // Concurrent assertions
   property full_check;
	  @(posedge clk_rstn_intrf.clk) disable iff (!clk_rstn_intrf.rst)
		items_count == 3 |-> (full_o && ~empty_o);
   endproperty // full_check
   
   full_check_assert : assert property (full_check);
   
   property empty_check;
	  @(posedge clk_rstn_intrf.clk) disable iff (!clk_rstn_intrf.rst)
		items_count == 0 |-> (empty_o && ~full_o);
   endproperty // empty_check

   empty_check_assert : assert property (empty_check);

   // Make sure that data inserted on previous cycle was stored
   // Write index is taken from 2 ticks back, due to the fact that
   // it changes at the same time with data_i.
   property store_check;
	  @(posedge clk_rstn_intrf.clk) disable iff (!clk_rstn_intrf.rst)
		(write_en_i && ~full_o) |=> ($past(data_i) == mem[($past(write_index, 2))]);
   endproperty // store_check

   store_check_assert : assert property (store_check);
   
endmodule
