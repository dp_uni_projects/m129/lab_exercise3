// `timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/15/2020 12:38:11 PM
// Design Name: 
// Module Name: counter
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module counter #(WIDTH = 5)
   (
	input logic [WIDTH-1:0]  data_i,
	input logic 			 load_i,
	input logic 			 en_i,
	input logic 			 clk_i,
	input logic 			 rstn_i,
	output logic [WIDTH-1:0] count_o
    );

   always_ff @(posedge clk_i, negedge rstn_i)
	 begin
		if (!rstn_i)			  // If reset == 0, active low
		  count_o <= 0;			  // Reset counter
		else 
		  if(en_i)			  // If enable signal is on 
			count_o <= count_o + 1; // Count up from previous signal
		  else if(load_i)			  // If load signal is on
			count_o <= data_i;	  // Load value to counter
	 end
endmodule
