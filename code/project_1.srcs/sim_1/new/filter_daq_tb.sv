
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/28/2020 01:35:53 PM
// Design Name: 
// Module Name: filter_daq_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module filter_daq_tb();
`timescale 1ns / 1ps   
   logic clk = 1'b0;
   logic rst = 1'b1;
   logic [15:0] data_i = 0;
   logic 		data_av_i = 1'b0;
   logic [15:0] avg_o;
   
   clk_rst_if bus();
   
   assign bus.clk = clk;
   assign bus.rst = rst;

`define PERIOD 10

   always
	 begin : CLK_PROCESS
		#(`PERIOD/2) clk = ~clk;		
	 end : CLK_PROCESS   

   filter_daq filter_daq_inst
	 (
	  .data_i(data_i),
	  .data_av_ai(data_av_i),
	  .clk_rstn_intrf(bus),
	  .avg_o(avg_o)
	  );

   // Signal monitor
   initial
	 begin
		$timeformat ( -9, 0, "ns", 3 ) ;
		$monitor ( "%t\tdata_av_i=%b, data_i=0x%h, avg_o=0x%h",
				   $time, data_av_i, data_i, avg_o);
		
		#(`PERIOD * 99)			// wait for 99 * 10 ns
		$display ( "TEST TIMEOUT" );
		$finish;					// Stops testbench
	 end

   // Median value output check block
   int median_value, d0, d1, d2;
   always
   	 begin : MEDIAN_CHECK
   		@(posedge clk)
   		  begin
   			 median_value = 0;
   			 d0 = 		filter_daq_inst.median_filter_inst.data0_o;
   			 d1 = 		filter_daq_inst.median_filter_inst.data1_o;
   			 d2 = 		filter_daq_inst.median_filter_inst.data2_o;

   			 if(d0 > d1)
   			   if(d1 > d2)
   				 median_value = d1;
   			   else
   				 if(d0 > d2)
   				   median_value = d2;
   				 else
   				   median_value = d0;
   			 else
   			   if(d1 > d2)
   				 if(d0 > d2)
   				   median_value = d0;
   				 else
   				   median_value = d2;
   			   else
   				 median_value = d1;

   			 if(median_value != filter_daq_inst.median_filter_inst.data_o)
   			   $error("Invalid median %d calculated from values %d, %d, %d",filter_daq_inst.median_filter_inst.data_o,d0,d1,d2);
   		  end
   	 end : MEDIAN_CHECK
   
   
   // Always block which calculates the average in software
   int median_acc = 0;
   int i = 0;
   always
	 begin : AVG_CHECK
		@(posedge filter_daq_inst.fsm_wr_inst.control_we_o)
		  begin
			 median_acc += filter_daq_inst.fsm_wr_inst.data_o;
			 i+=1;
			 if(i >= 8)
			   begin
				  median_acc = median_acc/8;
				  $info("Average calculated = %d", median_acc);
			   end
		  end
	 end : AVG_CHECK

   // Actual Testbench
   initial
	 begin : TB
		int input_data[0:7] = {1500, 100, 10, 40000, 300, 1100, 35000, 2000};
		rst = 1'b0;
		#(`PERIOD);
		@(negedge clk)
		rst = 1'b1;	

		@(posedge clk);
		#3						// Make input data kind of async
		foreach (input_data[i])
		  begin
			 data_av_i = 1'b1;			 
			 #(`PERIOD);
			 data_i = input_data[i];
			 data_av_i = 1'b0;
			 #(`PERIOD);
		  end
		data_av_i = 1'b0;		

		#(`PERIOD * 40);		// Wait for hardware to do its thing

		// Compare hardware and software averages
		if(avg_o != median_acc)
		  begin
			 $error("Invalid average calculated (%d). It should be %d", avg_o, median_acc);
		  end
		else
		  begin
			 $info("Validated average calculated by hardware (%d)", avg_o);
		  end
		
		$display("ALL TESTS PASSED");
		$finish(0);
	 end : TB
   
   
endmodule
