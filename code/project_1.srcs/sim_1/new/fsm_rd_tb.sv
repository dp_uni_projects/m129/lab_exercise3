
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/01/2020 05:10:17 PM
// Design Name: 
// Module Name: fsm_rd_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module fsm_rd_tb();
   timeunit 1ns;
   timeprecision 100ps;   

`define PERIOD 10

   logic clk = 1'b0;
   logic rst = 1'b1;
   
   logic control_i = 1'b0;
   logic [15:0] rd_data_i;
   logic [15:0] avg_o;
   logic [2:0] 	addr_o;
   
   clk_rst_if bus();
   assign bus.clk = clk;
   assign bus.rst = rst;
   
   always
	 begin : CLK_PROCESS
		#(`PERIOD/2);
		clk = ~clk;
	 end : CLK_PROCESS


   // Signal monitor
   initial
	 begin
		$timeformat ( -9, 0, "ns", 3 ) ;
		$monitor ( "%t\trd_data_i=0x%h, state=%0s, addr=%d",
				   $time, rd_data_i, fsm_rd_inst.fsm_state.name(),
				   addr_o);
		
		#(`PERIOD * 99)			// wait for 99 * 10 ns
		$display ( "TEST TIMEOUT" );
		$finish;					// Stops testbench
	 end
   
   fsm_rd fsm_rd_inst
	 (
	  .clk_rstn_intrf(bus),		// CLK RST bus
	  .control_i(control_i),	// Signal that fsm_wr is done
	  .rd_data_i(rd_data_i),	// Data from RAM
	  .avg_o(avg_o),			// Final result
	  .addr_o(addr_o)			// RAM Address to read from
	  );

   initial 
	 begin: TB
		rst = 1'b0;
		control_i = 1'b0;
		rd_data_i = 16'd0;
		@(posedge clk);
		rst = 1'b1;
		rd_data_i = 16'd0;	// Write data without data available signal		
		
		#(`PERIOD);

		control_i = 1'b1;		// Enable data available signal
		for(int i = 0; i<10; i++)
		  begin
			 rd_data_i = i;
			 #(`PERIOD*2);
		  end
		
		#(`PERIOD*3);
		$display("ALL TESTS PASSED");
		$finish();
	 end: TB

endmodule
