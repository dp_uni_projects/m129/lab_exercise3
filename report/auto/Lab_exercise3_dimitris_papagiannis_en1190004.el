(TeX-add-style-hook
 "Lab_exercise3_dimitris_papagiannis_en1190004"
 (lambda ()
   (setq TeX-command-extra-options
         "-output-directory=build")
   (TeX-run-style-hooks
    "uoa_template"
    "soul")
   (TeX-add-symbols
    "documentTitle"))
 :latex)

