module picoblaze_top_tb;
timeunit 1ns;
timeprecision 100ps;

logic         rst_i;
logic         clk_i = 1'b1;
logic         interrupt_req_i;

`define PERIOD 10

always
    #(`PERIOD/2) clk_i = ~clk_i;

picoblaze_top pico_blaze_top_inst(
	.cpu_rst_i             ( rst_i ),
	.clk_i                 ( clk_i ),
	.interrupt_req_i       ( interrupt_req_i ),
	.in_porta_i            ( 8'h25 ),
	.in_portb_i            (  ),
	.in_portc_i            (  ),
	.in_portd_i            (  ),
	.interrupt_ack_o       (  ), 
	.write_strobe_o        (  ),
	.k_write_strobe_o      (  ),
	.read_strobe_o         (  ),
	.out_portw_o           (  ),
	.out_portx_o           (  ),
	.out_porty_o           (  ),
	.out_portz_o           (  ),
	.out_portk0_o          (  ),
	.out_portk1_o          (  ),
	.port_id_o             (  )
);




initial begin
rst_i = 1;
#10
rst_i = 0;
#400
interrupt_req_i = 1;
#20
interrupt_req_i = 0;
end
endmodule : picoblaze_top_tb

